cTesting starten:

regbus.py 3 easytouch.xml -vvvv -l test.log

regbus.py python script
3 = COM3 Port
-vvvv Wie viele Informationen auf Konsole geprintet werden sollen

-l test.log = Logfile erstellen, mit Name "test"

  endless = 1 -> unendlich oft xml durchgegehen und schauen, ob request auf endless ist oder not.
  Zeile 1 - letzte Zeile durchgehen...
  
  delay = Anfangsdelay, bevor ein Request ausgesendet wird.
 
  cyclic = zyklus, in welchem Taskinktime messages ausgesendet werden sollen.



 <request name="Read FirmwareVersion" value="01 03 0000 0020" endless="0" stoponerror="1" delay="0" cyclic="0"> 
 <verify name="Verify Length" length="67"/>
 </request>
 

 01 = Adresse von Device, 03 = Read Holding Register, 0000 = Adresse HR, 0020 = 32Register (Words) lesen
 length = empfangene Bytes: 1B function code, 1B count, 1B Adresse v. Device + 32W Read -> 1 + 1 + 1 + 64B = 67B

 <request name="WriteLEDs" value="01 10 2100 0001 02 00 00" endless="1" stoponerror="1" delay="0" cyclic="5000">
 <verify name="Verify Length" length="6"/>
 </request>

 01 = Adresse, 10 = Write multiple register, 2100 = Adresse HR, 0001 = Anzahl Words to Write, 02 = Anzahl Bytes, 
 00 00 Data to be written
 length = empfangen Bytes: 1B function code, 2B starting Address, 2B Quantity Registers, 1B Adresse v. Device