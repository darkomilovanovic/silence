@echo off

ver
PATH 
for /f "delims=" %%a in ('where fsutil') do @set fsUtil=%%a

set binFile=%1
set padFile=%binFile:~0,-4%.pad
set imgFile=%binFile:~0,-4%.img
set heaFile=%binFile:~0,-4%.hea
set path=%~dp0
set imgEndFile="%path%image.pattern"

set imgSize=%2
set imgEndString="end of image"
set heaSize=128

set binSize=%~z1
FOR /F "usebackq" %%A IN ('%imgEndFile%') DO set endSize=%%~zA
set /a diffSize=%imgSize%-%binSize%-%endSize%

del %imgFile% >NUL 2>&1
del %padFile% >NUL 2>&1
del %heaFile% >NUL 2>&1

%fsUtil% file createnew %padFile% %diffSize%
%fsUtil% file createnew %heaFile% %heaSize%

copy /B %heaFile%+%binFile%+%padFile%+%imgEndFile% %imgFile%
