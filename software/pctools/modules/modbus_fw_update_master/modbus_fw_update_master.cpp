// 
//============================================================================== 
//  Copyright (c) Fr. Sauter AG 
//============================================================================== 
/** \file  bamo_fw_update_main.cpp 
  @author  Beat Morf
   @brief  Main application battery monitor firmware upate
*******************************************************************************/ 

#include <stdio.h>
#include <stdint.h>
#ifndef _MSC_VER
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "modbus.h"
#include "c_modbus_fw_update_master\c_modbus_fw_update_master.h"
#include "CmdLine\CmdLine.h"


/**
*/
void show_info() {
  printf("Battery Monitor Firmware Updater (bamo_fw_update), v1.0\n");
}

/**
*/
void show_help() {
  printf("Required Parameters:\n");
  printf("    -com [COM PORT]\n");
  printf("    -file [filename]\n");
  printf("Optional Parameters:\n");
  printf("    -baud [BAUDRATE]       default: 9600\n");
  printf("    -slave [ID]            default: 1\n");
  printf("    -v                     verbose output");
  printf("\n");
}

/**
*/
int main(int argc, char* argv[])
{
  uint8_t                                     result = 0;
  BOOL                                        show_syntax = FALSE;
  uint8_t                                     i;
  c_modbus_fw_update_master                   bamo_fw_update;
  CCmdLine                                    cmdLine;
  uint8_t                                     com_port;
  string                                      filename;
  c_modbus_fw_update_master::T_MODBUS_FW_UPDATE_CONFIG   bamo_fw_update_config;

  show_info();

  // parse argc,argv 
  if (cmdLine.SplitLine(argc, argv) < 1) {
    show_help();
    exit(1);
  }
  else if (cmdLine.HasSwitch("-h")) {
    show_help();
    exit(0);
  }

  // get the required arguments
  try
  {  
      // if any of these fail, we'll end up in the catch() block
      com_port = atoi(cmdLine.GetArgument("-com", 0).c_str());
      filename = cmdLine.GetArgument("-file", 0);
  }
  catch (...)
  {
      // one of the required arguments was missing, abort
      show_help();
      exit(1);
  }

  // get the optional parameters
  bamo_fw_update_config.baud = atoi(cmdLine.GetSafeArgument("-baud", 0, "115200").c_str());
  bamo_fw_update_config.slave_id = atoi(cmdLine.GetSafeArgument("-slave", 0, "1").c_str());

  // debugging
  if (cmdLine.HasSwitch("-v")) {
    bamo_fw_update.enable_debugging();
  }

  // serial port
  sprintf_s(bamo_fw_update_config.device, sizeof(bamo_fw_update_config.device), "\\\\.\\COM%d", com_port);

  // Configure ModBus interface
  printf("Initialize Battery Monitor update...\n");
  bamo_fw_update_config.parity = 'N';
  bamo_fw_update_config.data_bit = 8;
  bamo_fw_update_config.stop_bit = 2;
  result = bamo_fw_update.init(&bamo_fw_update_config, filename.c_str());

  // Connect to the ModBus Slave
  if (0 == result) {
    printf("Connect to Battery Monitor...\n");
    for (i=0; i<3; i++) {
      result = bamo_fw_update.connect();
      if (0 == result) {
        i = 3;
      }
      else if (c_modbus_fw_update_master::FWU_DEVICE_NOT_READY == bamo_fw_update.getLastError()) {
        bamo_fw_update.reset_device();
      }
    }
  }

  // Update
  if (0 == result) {
    printf("Update Battery Monitor...");
    result = bamo_fw_update.update();
  }

  // Wait until update has finished
  while ((0 == result) && (TRUE == bamo_fw_update.update_running())) {
    Sleep(1000);
    printf(".");
    result = bamo_fw_update.getLastError();
  }
  printf("\n");

  if (0 == result) {
    printf("Battery Monitor update finished successfully!\n");
  }
  else {
    printf("Battery Monitor update failed with ERROR %d!\n", bamo_fw_update.getLastError());
  }

  bamo_fw_update.close();

  return result;
}


