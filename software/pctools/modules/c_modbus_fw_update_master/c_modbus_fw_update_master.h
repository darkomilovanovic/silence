// 
//============================================================================== 
//  Copyright (c) Fr. Sauter AG 
//============================================================================== 
/** \file  c_modbus_fw_update_master.h 
  @author  Beat Morf
   @brief  Module for battery monitor firmware update (master)
*******************************************************************************/ 
#ifndef __C_BAMO_FW_UPDATE_H__ 
#define __C_BAMO_FW_UPDATE_H__ 


/** Class description.  
*/ 
class c_modbus_fw_update_master { 
public:
  typedef enum {
    FWU_NO_ERROR                = 0,
    FWU_DEVICE_ERROR            = -1,
    FWU_SLAVE_ID_ERROR          = -2,
    FWU_CONNECT_ERROR           = -3,
    FWU_MODBUS_CONNECTION_ERROR = -4,
    FWU_DEVICE_NOT_READY        = -5,
    FWU_RESET_ERROR             = -6,
    FWU_INVALID_BINARY_FILE     = -7,
    FWU_WRITE_HR_ERROR          = -8,
    FWU_READ_HR_ERROR           = -9,
    FWU_DEVICE_UPDATE_FAILURE   = -10,
    FWU_READ_BIN_FILE_ERROR     = -11,
    FWU_PROGRAMMING_ERROR       = -12,
    FWU_RESTART_ERROR           = -13,
    FWU_UPDATE_THREAD_ERROR     = -14,
    FWU_UPDATE_RUNNING          = -15,
    FWU_FW_RESET_ERROR          = -16
  } T_MODBUS_FW_UPDATE_ERROR;

  typedef struct {
    char          device[20];
    int           baud;
    char          parity;
    int           data_bit;
    int           stop_bit;
    int           slave_id;
  } T_MODBUS_FW_UPDATE_CONFIG;


private:
  modbus_t                *ctx;
  T_MODBUS_FW_UPDATE_ERROR error;
  uint8_t                 debug_flag;
  HANDLE                  h_binary_file;
  HANDLE                  h_update_thread;
  DWORD                   update_thread_id;

public: 
  c_modbus_fw_update_master();

  int init(T_MODBUS_FW_UPDATE_CONFIG* config, const char *filename);
  int connect();
  int reset_device();
  int update();
  int update_blocking();
  BOOL update_running();
  void close();


  T_MODBUS_FW_UPDATE_ERROR getLastError() { return error; };

  uint32_t calculate_crc32(uint32_t* data, uint32_t length);
  void enable_debugging() { debug_flag = 1; };
  void disable_debugging() { debug_flag = 0; };

private:
  uint32_t crc32(uint32_t Crc, uint32_t Data);
  int set_error(T_MODBUS_FW_UPDATE_ERROR err) { error = err; return (int)error; };
  int open_binary_file(LPCTSTR filename);

};/* c_modbus_fw_update_master */ 
#endif 
/******************************************************************************* 
                             END_OF_FILE 
*******************************************************************************/ 
