// 
//============================================================================== 
//  Copyright (c) Fr. Sauter AG 
//============================================================================== 
/** \file  c_modbus_fw_update_master.cpp 
  @author  Beat Morf
   @brief  Module for battery monitor firmware upate
*******************************************************************************/ 
#include <stdint.h>
#include <stdio.h> 
#include <stdlib.h> 

/* Includes ------------------------------------------------------------------*/ 
#include "modbus.h"
#include "c_modbus_fw_update_master\c_modbus_fw_update_master.h" 
#include "c_device_update\c_device_update.hpp" 
#include "c_update_control\c_update_control.hpp"
#include "c_module\c_module.hpp"
#include "c_modbus\c_modbus.hpp"

static DWORD WINAPI UpdateThreadFunction(LPVOID lpParam);

/* Public function -----------------------------------------------------------*/ 

/**
*/
c_modbus_fw_update_master::c_modbus_fw_update_master()
{
  ctx = NULL;
  error = FWU_NO_ERROR;
  debug_flag = 0;
  h_binary_file = NULL;
}

/**
*/
uint32_t c_modbus_fw_update_master::calculate_crc32(uint32_t* data, uint32_t length)
{
  uint32_t i;
  uint32_t crc = 0;

  for (i=0; i<length; i++) {
    crc = crc32(crc, data[i]);
  }

  return crc;
}

/**
*/
uint32_t c_modbus_fw_update_master::crc32(uint32_t Crc, uint32_t Data)
{
  int i;

  Crc = Crc ^ Data;

  for(i=0; i<32; i++)
    if (Crc & 0x80000000)
      Crc = (Crc << 1) ^ 0x04C11DB7; // Polynomial used in STM32
    else
      Crc = (Crc << 1);

  return(Crc);
}

/**
*/
int c_modbus_fw_update_master::init(T_MODBUS_FW_UPDATE_CONFIG* config, const char* filename)
{
  int result = set_error(FWU_NO_ERROR);

  /* RTU */
  ctx = modbus_new_rtu(config->device, config->baud, config->parity, config->data_bit, config->stop_bit);
  if (ctx == NULL) {
    result = set_error(FWU_DEVICE_ERROR);
  }
  else {
    modbus_set_debug(ctx, debug_flag);

    if (0 != modbus_set_slave(ctx, config->slave_id)) {
      result = set_error(FWU_SLAVE_ID_ERROR);
    }
    else if (0 != modbus_connect(ctx)) {
      result = set_error(FWU_CONNECT_ERROR);
    }
  }

  if (0 == result) {
    result = open_binary_file(filename);
  }

  return result;
}

/**
*/
int c_modbus_fw_update_master::connect()
{
  int result = set_error(FWU_NO_ERROR);
  uint16_t fw_state;

  if (NULL != ctx) {
    if (1 != modbus_read_registers(ctx, C_MODBUS::HREG_ADDR_FW_UPDATE_STATE, 1, &fw_state)) {
      result = set_error(FWU_MODBUS_CONNECTION_ERROR);
    }
    else if (C_UPDATE_CONTROL::FW_STATE_READY != fw_state) {
      result = set_error(FWU_DEVICE_NOT_READY);
    }
  }

  return result;
}

/**
*/
int c_modbus_fw_update_master::reset_device()
{
  int result = set_error(FWU_NO_ERROR);
  uint16_t reset = C_UPDATE_CONTROL::FW_STATE_RESET;

  if (1 != modbus_write_register(ctx, C_MODBUS::HREG_ADDR_FW_UPDATE_STATE, reset)) {
    result = set_error(FWU_RESET_ERROR);
  }

  return result;
}

/**
*/
int c_modbus_fw_update_master::update_blocking()
{
  uint32_t  data[C_UPDATE_CONTROL::UPDATE_FW_PART_LENGTH / 4];
  uint16_t  status;
  uint32_t  crc;
  uint32_t  crc_total = 0xFFFFFFFF;
  uint32_t  i,j = 0;
  DWORD     bytesRead = 1;
  BOOL      readFinished;
  BOOL      erasing = TRUE;
  uint8_t   fw_part_length = C_UPDATE_CONTROL::UPDATE_FW_PART_LENGTH;
  uint8_t   hr_fw_length = C_UPDATE_CONTROL::UPDATE_FW_PART_LENGTH / 2;
  int       result = 0;
  DWORD     start;

  while ((0 == result) && (0 < bytesRead)) {
    // Clear data array
    for (i=0; i<120/4; i++) {
      data[i] = 0xFFFFFFFF;
    }

    // Read out file
    readFinished = ReadFile(h_binary_file,
      (uint8_t*)data,
      C_UPDATE_CONTROL::UPDATE_FW_PART_LENGTH,
      &bytesRead, NULL);

    if (readFinished && (0 < bytesRead))
    {
      // Write data
      if (hr_fw_length != modbus_write_registers(ctx, C_MODBUS::HREG_ADDR_FW_UPDATE, hr_fw_length, (uint16_t*)data)) {
        result = set_error(FWU_WRITE_HR_ERROR);
      }

      // Calculate and write crc32
      if (0 == result) {
        crc = 0xFFFFFFFF;
        for (i=0; i<(120/4); i++) {
          crc = crc32(crc, data[i]);
          crc_total = crc32(crc_total, data[i]);
        }

        if (2 != modbus_write_registers(ctx, C_MODBUS::HREG_ADDR_FW_UPDATE_CRC_LW, 2, (uint16_t*)&crc)) {
          result = set_error(FWU_WRITE_HR_ERROR);
        }
      }

      // Activate update mechanism
      if (0 == result) {
        status = C_UPDATE_CONTROL::FW_STATE_NEW_DATA;
        if (1 != modbus_write_register(ctx, C_MODBUS::HREG_ADDR_FW_UPDATE_STATE, status)) {
          result = set_error(FWU_WRITE_HR_ERROR);
        }

        if ((0 == result) && (TRUE == erasing)) {
          // Sleep for 1 second until the flash is erased!
          Sleep(1000);
          erasing = FALSE;
        }
      }

      // Wait until BaMo is ready
      if (0 == result) {
        start = GetTickCount();

        while ( (C_UPDATE_CONTROL::FW_STATE_WAITING != status) &&
                (1000 > (GetTickCount() - start)) &&
                (0 == result) )
        {
          if (1 != modbus_read_registers(ctx, C_MODBUS::HREG_ADDR_FW_UPDATE_STATE, 1, &status)) {
            result = set_error(FWU_READ_HR_ERROR);
          }
        }

        if ( (0 == result) && (C_UPDATE_CONTROL::FW_STATE_FAILURE == status) ) {
          result = set_error(FWU_DEVICE_UPDATE_FAILURE);
        }
      }
    }
    else if (!readFinished) {
      result = set_error(FWU_READ_BIN_FILE_ERROR);
    }
  }

  // Update the flash
  if (0 == result) {
    if (2 != modbus_write_registers(ctx, C_MODBUS::HREG_ADDR_FW_UPDATE_CRC_LW, 2, (uint16_t*)&crc_total)) {
      result = set_error(FWU_WRITE_HR_ERROR);
    }
    else {
      status = C_UPDATE_CONTROL::FW_STATE_PROGRAM;
      modbus_write_register(ctx, C_MODBUS::HREG_ADDR_FW_UPDATE_STATE, status);
    }
  }

  if (0 == result) {
    // Wait until the device is in reset mode
    start = GetTickCount();
    while (1 == modbus_read_registers(ctx, C_MODBUS::HREG_ADDR_FW_UPDATE_STATE, 1, &status)) {
      Sleep(100);
      if (3000 < (GetTickCount() - start)) {
        result = set_error(FWU_FW_RESET_ERROR);
        break;
      }
    }
  }


  if (0 == result) {
    // Wait until the device has restarted
    start = GetTickCount();
    while (1 != modbus_read_registers(ctx, C_MODBUS::HREG_ADDR_FW_UPDATE_STATE, 1, &status)) {
      Sleep(100);
      if (10000 < (GetTickCount() - start)) {
        result = set_error(FWU_RESTART_ERROR);
        break;
      }
    }
  }

  return result;
}

/**
*/
int c_modbus_fw_update_master::open_binary_file(LPCTSTR filename)
{
  int result = set_error(FWU_NO_ERROR);

  h_binary_file = CreateFile(filename,
        GENERIC_READ,
        0,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL);

  if (INVALID_HANDLE_VALUE == h_binary_file) {
    result = set_error(FWU_INVALID_BINARY_FILE);
  }

  return result;
}

/**
*/
void c_modbus_fw_update_master::close()
{
  modbus_close(ctx);
  modbus_free(ctx);
}

/**
*/
int c_modbus_fw_update_master::update()
{
  int result = set_error(FWU_NO_ERROR);

  h_update_thread = CreateThread( 
      NULL,
      0,
      UpdateThreadFunction,
      this,
      0,
      &update_thread_id);
  if (NULL == h_update_thread) {
    result = set_error(FWU_UPDATE_THREAD_ERROR);
  }

  return result;
}

/**
*/
BOOL c_modbus_fw_update_master::update_running()
{
  BOOL running = TRUE;
  DWORD thread_status;

  if (0 == GetExitCodeThread(h_update_thread, &thread_status)) {
    set_error(FWU_UPDATE_THREAD_ERROR);
    running = FALSE;
  }
  else if (STILL_ACTIVE != thread_status) {
    set_error((T_MODBUS_FW_UPDATE_ERROR)thread_status);
    running = FALSE;
  }

  return running;
}


/* Private function ----------------------------------------------------------*/ 
static DWORD WINAPI UpdateThreadFunction(LPVOID lpParam)
{ 
  DWORD result;

  c_modbus_fw_update_master *bamo_fw_update = (c_modbus_fw_update_master*)lpParam;

  result = bamo_fw_update->update_blocking();

  ExitThread(result);
  return 0; 
} 


/******************************************************************************* 
                             END_OF_FILE 
*******************************************************************************/ 
