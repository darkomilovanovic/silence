/** \defgroup C_MODBUS C_MODBUS_PackageTitle
 *
 * \brief This is the implementation of the modbus application layer
 */


#include "c_module/c_module.hpp"
#include "c_usart/c_usart.hpp"
#include "c_modbus_frame/c_modbus_frame.hpp"
#include "c_modbus/c_modbus.hpp"

#include "modbus/modbus.attr"

C_MODBUS::C_MODBUS(C_USART *usart)
{
  enabled = true;
  
  myModbusRtu = usart;
  
  myModbusRtu->Enable();
  
  for (int i = 0; i < HoldingRegistersLastItem; i++)
    s_modbusHRValues[i] = s_modbusHRDefaultValues[i];
  SetModbusMode(SLAVE_MODE);
  myModbusMasterState = MODBUS_READY;
  myMasterTimer.SetDuration(25);
  return;
}

void C_MODBUS::SetModbusMode(MODBUS_MODE modbusMode)
{
  myModbusMode = modbusMode;
  myReadHRRegister = 0xFFFF;
  myWriteHRRegister = 0xFFFF;
  myMasterTimer.Stop();
}

void C_MODBUS::Handler(void)
{
  uint16_t length;
  if(myMasterTimer.GetState() == C_TIMER::STATE_OVER)
  {
    myModbusMasterState = MODBUS_TIMEOUT;
    SetModbusMode(SLAVE_MODE);
  }
  if (true == enabled) 
  {
    if (0 < myModbusRtu->FrameAvailable()) 
    {
      length = myModbusRtu->GetFrame(myModbusFrame.GetData(), myModbusFrame.GetLength());
      myModbusFrame.SetCount(length);
      if (0 == myModbusFrame.Verify()) 
      {
        HandleFrame();
      }
      myModbusMasterState = MODBUS_READY;
      SetModbusMode(SLAVE_MODE);
    }
  }
  
  return;
}

void C_MODBUS::HandleFrame(void)
{
  if (DEVICE_ADDRESS == myModbusFrame.GetAddress()) {
    if(myModbusMode == MASTER_MODE)
    {
      switch(myModbusFrame.GetFunctionCode())
      {
       case MODBUS_READ_HOLDING_REGISTERS:
         HandleResponseReadHoldingRegister((uint8_t)(DEVICE_EASY_TASTER), myReadHRRegister);
       break;
       case MODBUS_WRITE_MULTIPLE_REGISTERS:
         HandleResponseWriteHoldingRegister((uint8_t)(DEVICE_EASY_TASTER), myWriteHRRegister);
         break;
       default:
         break;
      }
    }
    else
    {
      switch(myModbusFrame.GetFunctionCode())
      {
        case MODBUS_READ_HOLDING_REGISTERS:
        HandleRequestReadHoldingRegister();
        break;
      case MODBUS_WRITE_MULTIPLE_REGISTERS:
        HandleRequestWriteMultipleRegisters();
        break;
      case MODBUS_WRITE_SINGLE_REGISTER:
        HandleWriteSingleRegister();
        break;
      default:
        myModbusFrame.SetErrorCode(ecILLEGALFUNCTION);
        myModbusFrame.GenerateCRC();
        myModbusRtu->SetFrame(myModbusFrame.GetData(), myModbusFrame.Count());
        break;
      }
    }
  }  
  myModbusFrame.Reset();
}

void C_MODBUS::HandleResponseReadHoldingRegister(uint8_t HandleDeviceType, uint16_t HRRegister)
{ //Response auf Master Request Read Holding Register
  uint8_t slaveAddress = myModbusFrame.GetSlaveAddress();
  uint8_t slaveFunction = myModbusFrame.GetSlaveFunction();
  uint16_t valueHR = myModbusFrame.GetSlaveHRValue();
  uint16_t quantity = myModbusFrame.GetSlaveHRQuantityReadHR();
  quantity = quantity / 2;
  int index = 0;
  
   if (slaveAddress == HandleDeviceType && 
       slaveFunction == MODBUS_READ_HOLDING_REGISTERS && 
       quantity == 1){
    // Gibt es diese Register in der Tabelle s_modbusHRAddresses[]? 
    index = VerifyHoldingRegisters(HRRegister, (quantity));
  
    if (0 <= index) 
    {
       for (int i=0; i<quantity; i++) 
      {
        s_modbusHRValues[index] = valueHR;
        s_modbusHRSignedFlags[index] = MODIFIED;
      }
    }
  }
}

void C_MODBUS::HandleResponseWriteHoldingRegister(uint8_t HandleDeviceType, uint16_t HRRegister)
{ //Response auf Master Request Write Holding Register
  uint8_t slaveAddress = myModbusFrame.GetSlaveAddress();
  uint8_t slaveFunction = myModbusFrame.GetSlaveFunction();
  uint16_t HRstartingAddress = myModbusFrame.GetSlaveHRStartAddress();
  uint16_t quantity = myModbusFrame.GetSlaveHRQuantityWriteHR();

  if (slaveAddress == HandleDeviceType && 
      slaveFunction == MODBUS_WRITE_MULTIPLE_REGISTERS && 
      HRRegister == HRstartingAddress &&
      quantity == 2)
  {}
}

void C_MODBUS::HandleRequestReadHoldingRegister(void)        // lesen  s_modbusHRValues[]...
  { //Request vom MAster: (Slave) Read Holding Register
  uint16_t address = myModbusFrame.GetHRStartAddress();
  uint16_t quantity = myModbusFrame.GetHRQuantity();
  int index = 0;
  uint16_t value;
  
  if(myModbusFrame.GetCount() != 8)
    myModbusFrame.SetErrorCode(ecILLEGALDATAVALUE);
  else
  {   
    // Gibt es diese Register in der Tabelle s_modbusHRAddresses[]? 
    index = VerifyHoldingRegisters(address, quantity);
    
    if (0 <= index) {
      myModbusFrame.SetByteQuantity(quantity * 2);
      for (int i=0; i<quantity; i++) {
        //value = ByteSwap(s_modbusHRValues[index + i]);
        value = s_modbusHRValues[index + i];
        myModbusFrame.WriteU16(value);
      }
    }
    else {  
      myModbusFrame.SetErrorCode(ecILLEGALDATAADDRESS);
    }
  }
  myModbusFrame.GenerateCRC();
  myModbusRtu->SetFrame(myModbusFrame.GetData(), myModbusFrame.Count());
}

void C_MODBUS::HandleRequestWriteMultipleRegisters(void)      // schreiben  s_modbusHRValues[]...      *** noch zu testen!!! ***
  { //Request vom Master: (Slave) Write Multiple Holding Register
  uint16_t address = myModbusFrame.GetHRStartAddress();
  uint16_t quantity = myModbusFrame.GetHRQuantity();
  uint16_t byteQuantity = myModbusFrame.GetByteQuantity();
  int index = 0;
  uint16_t value;
  
  if(myModbusFrame.GetCount() != byteQuantity + 9)
    myModbusFrame.SetErrorCode(ecILLEGALDATAVALUE);
  else
  {    
    // Gibt es diese Register in der Tabelle s_modbusHRAddresses[]? 
    index = VerifyHoldingRegisters(address, quantity);
    
    if (0 <= index) {
      for (int i=0; i<quantity; i++) {
        myModbusFrame.Read((uint8_t*)&value, 2);
        s_modbusHRValues[index + i] = ByteSwap(value);
        //s_modbusHRValues[index + i] = value;
        s_modbusHRSignedFlags[index+i] = MODIFIED;
      }
      myModbusFrame.SetCount(6);
    }
    else {  
      myModbusFrame.SetErrorCode(ecILLEGALDATAADDRESS);
    }
  }
  myModbusFrame.GenerateCRC();
  myModbusRtu->SetFrame(myModbusFrame.GetData(), myModbusFrame.Count());
}
void C_MODBUS::ReadHRRequest(uint16_t deviceType, uint16_t HRPosition)
{
  uint16_t NumberOfRegisters = 1;
  myMasterTimer.SetDuration(25);
  myMasterTimer.Start();
  myModbusMasterState = MODBUS_BUSY;
  myReadHRRegister = GetHRAddress(HRPosition);
  myModbusFrame.SetSlaveAddress(deviceType);                       // 1 Byte
  myModbusFrame.SetSlaveFunctionCode(MODBUS_READ_HOLDING_REGISTERS);    // 1 Byte
  myModbusFrame.WriteU16(myReadHRRegister);                   // 2 Bytes
  myModbusFrame.WriteU16(NumberOfRegisters);           // 2 Bytes
  myModbusFrame.GenerateCRC();                                     // 2 Bytes
  myModbusRtu->SetFrame(myModbusFrame.GetData(), myModbusFrame.Count());
}
void C_MODBUS::WriteHRRequest(uint8_t deviceType, uint16_t HRPosition, uint16_t dataToWrite)
{
  uint8_t numberOfBytes = 2;
  myMasterTimer.SetDuration(25);
  myMasterTimer.Start();
  myModbusMasterState = MODBUS_BUSY;
  myWriteHRRegister = GetHRAddress(HRPosition);
  myModbusFrame.SetSlaveAddress(deviceType);
  myModbusFrame.SetSlaveFunctionCode(MODBUS_WRITE_MULTIPLE_REGISTERS);
  myModbusFrame.WriteU16(myWriteHRRegister);
  myModbusFrame.WriteU16(((uint16_t)numberOfBytes/2));
  myModbusFrame.WriteU8(&numberOfBytes, 1);
  myModbusFrame.WriteU16(dataToWrite);
  myModbusFrame.GenerateCRC();
  myModbusRtu->SetFrame(myModbusFrame.GetData(), myModbusFrame.Count());
}

void C_MODBUS::HandleWriteSingleRegister(void)      // schreiben  s_modbusHRValues[]...      *** noch zu testen!!! ***
{
  uint16_t address = myModbusFrame.GetHRStartAddress();
  uint16_t quantity = 1;
  int index = 0;
  uint16_t value;
  
  if(myModbusFrame.GetCount() != 8)
    myModbusFrame.SetErrorCode(ecILLEGALDATAVALUE);
  else
  {   
    // Gibt es diese Register in der Tabelle s_modbusHRAddresses[]? 
    index = VerifyHoldingRegisters(address, quantity);
    
    if (0 <= index) {
      myModbusFrame.Read((uint8_t*)&value, 2);
      s_modbusHRValues[index] = ByteSwap(value);
      //s_modbusHRValues[index] = value;
      s_modbusHRSignedFlags[index] = MODIFIED;
      myModbusFrame.SetCount(6);
    }
    else {  
      myModbusFrame.SetErrorCode(ecILLEGALDATAADDRESS);
    }
  }
  myModbusFrame.GenerateCRC();
  myModbusRtu->SetFrame(myModbusFrame.GetData(), myModbusFrame.Count());
}

int16_t C_MODBUS::VerifyHoldingRegisters(uint16_t address, uint16_t quantity)
{
  int16_t result = -1;
  int i;
  int j;
  
  for (i=0; i<sizeof(s_modbusHRAddresses); i++) {
    if (s_modbusHRAddresses[i] == address) {
      result = i;
      break;
    }
  }
  if ((result+quantity) <= HoldingRegistersLastItem ) 
  {   
    if (0 <= result)
    {
      for (j=0; j<quantity; j++) {
        if (s_modbusHRAddresses[i+j] != address+j) {
          break;
        }
      }
    }
  }  
  if (j != quantity) {
    result = -1;
  }

  return result;
}

uint16_t C_MODBUS::ByteSwap(uint16_t data) {
  uint16_t swapped = (data & 0x00FF) << 8;
  swapped |= (data & 0xFF00) >> 8;
  return swapped;
}

void C_MODBUS::SetHRValue(uint16_t indexHR, uint16_t data) {
  s_modbusHRValues[indexHR] = data;      // Schreibe Data an Adresse in Holding Register
}  

void C_MODBUS::SetHRValues(uint16_t indexHR, uint16_t *data, uint8_t length) {
  
  uint16_t address = s_modbusHRAddresses[indexHR];
  int16_t index = VerifyHoldingRegisters(address, length);
  
  if ((0 <= index) && (indexHR == index)) {
    for (int i=0; i<length; i++) 
    {
      s_modbusHRValues[index + i] = *data;
      data += 1;    
    }
  }
}

uint16_t C_MODBUS::GetHRValue(uint16_t addressHR)
{
  return s_modbusHRValues[addressHR];
}

uint8_t C_MODBUS::GetHRValueModified(uint16_t addressHR)
{
  if(s_modbusHRSignedFlags[addressHR] == MODIFIED)
    return MODIFIED;
  else
    return NOT_MODIFIED;
}

void C_MODBUS::EraseHRValueModifiedFlag(uint16_t addressHR)
{
  s_modbusHRSignedFlags[addressHR] = ERASE_FLAG_MODIFIED;
}
 
uint16_t C_MODBUS::GetHRAddress(uint16_t addressHR)
{
  return s_modbusHRAddresses[addressHR];
}
C_MODBUS::MODBUS_MASTER_STATE C_MODBUS::GetModbusMasterState(void)
{
  return myModbusMasterState;
}
void C_MODBUS::Enable(void) {
  enabled = true;
  myModbusRtu->SetMode(C_USART::MODE_MODBUS_RTU);
}

void C_MODBUS::Disable(void) {
  myModbusRtu->SetMode(C_USART::MODE_USART);
  enabled = false;
}
