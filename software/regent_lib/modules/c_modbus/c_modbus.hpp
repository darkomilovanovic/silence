/**
 * \class C_MODBUS
 *
 * \ingroup C_MODBUS
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_MODBUS implementation
 *
 * This class implements the modbus application layer
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_MODBUS_H
#define C_MODBUS_H


#define MODIFIED (uint8_t)0x01
#define NOT_MODIFIED (uint8_t)0x00
#define ERASE_FLAG_MODIFIED (uint8_t)0x00

#ifndef DEVICE_ADDRESS
  #define DEVICE_ADDRESS    0x01
#endif

#define DEVICE_EASY_TASTER 0x01
#define DEVICE_TYPE_EASY_TASTER 0x0001

#include "c_timer/c_timer.hpp"
class C_MODBUS : public C_MODULE
{
public:
  enum MODBUS_MODE {
    MASTER_MODE,
    SLAVE_MODE
  };
  
  enum MODBUS_MASTER_STATE {
    MODBUS_READY,
    MODBUS_BUSY,
    MODBUS_TIMEOUT
  };
  
private:
#if !defined (WIN32)
  C_MODBUS_FRAME      myModbusFrame;
  C_USART             *myModbusRtu;
#endif
  bool                enabled;
  MODBUS_MODE         myModbusMode;
  C_TIMER             myMasterTimer;
  uint16_t            myReadHRRegister;
  uint16_t            myWriteHRRegister;
  MODBUS_MASTER_STATE myModbusMasterState;
  

public:
  #include "modbus/modbus.vid"

  /// Create an C_MODBUS
#if !defined (WIN32)
  C_MODBUS(C_USART *usart);
#endif

  /// Delete an C_MODBUS
  ~C_MODBUS();

  /// Handler
  virtual void Handler(void);

  /// Execute the modbus function
  void HandleFrame(void);

  /// Read holding registers
  void HandleRequestReadHoldingRegister(void);

  /// Write multiple holding registers
  void HandleRequestWriteMultipleRegisters(void);
  
  /// Write single holding register
  void HandleWriteSingleRegister(void);

  /// Verify if the specified holding registers exist
  int16_t VerifyHoldingRegisters(uint16_t address, uint16_t quantity);
  
  /// Swap the 16bit holding register content
  uint16_t ByteSwap(uint16_t data);

  /// Set values in HR registers
  void SetHRValue(uint16_t indexHR, uint16_t data);
  
  /// Set values in Holding Registers
  void SetHRValues(uint16_t indexHR, uint16_t *data, uint8_t length);

  /// Enable the modbus handling
  void Enable(void);
  
  /// Disable the modbus handling
  void Disable(void);
  
  uint16_t GetHRValue(uint16_t addressHR);
  
  uint8_t GetHRValueModified(uint16_t addressHR);
  
  void EraseHRValueModifiedFlag(uint16_t addressHR);

  ///Functions used by EasyTasterMaster
  void HandleSlaveReadHoldingRegister(uint16_t HandleDeviceType, uint16_t HRRegister);
  void HandleResponseReadHoldingRegister(uint8_t deviceType, uint16_t HRPosition);
  void WriteHRRequest(uint8_t deviceType, uint16_t HRAdress, uint16_t dataToWrite);
  void ReadHRRequest(uint16_t HandleDeviceType, uint16_t HRRegister);
  void SetModbusMode(MODBUS_MODE modbusMode);
  void HandleResponseWriteHoldingRegister(uint8_t HandleDeviceType, uint16_t HRRegister);

  MODBUS_MASTER_STATE GetModbusMasterState(void);
  
  uint16_t GetHRAddress(uint16_t addressHR);
};  // end of class C_MODBUS

#endif  // C_MODBUS_H

