/**
 * \class C_YMODEM
 *
 * \ingroup C_YMODEM
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_YMODEM implementation
 *
 * This class implements a the standard module behaviour and interface
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_YMODEM_H
#define C_YMODEM_H

#include "c_usart/c_usart.hpp"
#include "c_timer/c_timer.hpp"


#define IS_AF(c)  ((c >= 'A') && (c <= 'F'))
#define IS_af(c)  ((c >= 'a') && (c <= 'f'))
#define IS_09(c)  ((c >= '0') && (c <= '9'))
#define ISVALIDHEX(c)  IS_AF(c) || IS_af(c) || IS_09(c)
#define ISVALIDDEC(c)  IS_09(c)
#define CONVERTDEC(c)  (c - '0')
#define CONVERTHEX_alpha(c)  (IS_AF(c) ? (c - 'A'+10) : (c - 'a'+10))
#define CONVERTHEX(c)   (IS_09(c) ? (c - '0') : CONVERTHEX_alpha(c))

class C_YMODEM
{

private:
  C_USART   *myUsart;
  C_TIMER   myTimer;

  uint32_t  myAppMirrorDestination;

  static const uint8_t PACKET_HEADER   = 3;
  static const uint8_t PACKET_TRAILER  = 2;
  static const uint8_t PACKET_OVERHEAD = PACKET_HEADER + PACKET_TRAILER;

  static const uint8_t PACKET_SEQNO_INDEX      = 1;
  static const uint8_t PACKET_SEQNO_COMP_INDEX = 2;

  static const uint16_t FILE_NAME_LENGTH = 256;
  static const uint16_t FILE_SIZE_LENGTH = 16;

  static const uint32_t NAK_TIMEOUT = 0x00100000;
  static const uint8_t MAX_ERRORS = 5;

  static const uint8_t TIMEOUT = 120;

  enum PACKET_SIZE {
    PACKET_SIZE_NORMAL = 128,
    PACKET_SIZE_1K     = 1024
  };

  enum PACKET_TYPE {
    SOH    = 0x01,  /* start of 128-byte data packet */
    STX    = 0x02,  /* start of 1024-byte data packet */
    EOT    = 0x04,  /* end of transmission */
    ACK    = 0x06,  /* acknowledge */
    NAK    = 0x15,  /* negative acknowledge */
    CA     = 0x18,  /* two of these in succession aborts transfer */
    ABORT1 = 0x41,  /* 'A' == 0x41, abort by user */
    CRC16  = 0x43,  /* Request 16Bit CRC */
    ABORT2 = 0x61   /* 'a' == 0x61, abort by user */
  };



public:

  /// Create an C_YMODEM
  C_YMODEM(C_USART *usart);

  /// Delete an C_YMODEM
  ~C_YMODEM();

  /// Handler
  virtual void Handler(void);

  /// Receive a file using the ymodem protocol
  int32_t ReceiveBlocking(uint8_t listen);

private:
  
  /// Receive byte from sender
  int32_t ReceiveByte(uint8_t *c, uint32_t timeout);

  /// Send a byte
  uint32_t SendByte(uint8_t listen, uint8_t c);

  /// Receive a packet from sender
  int32_t ReceivePacket (uint8_t *data, int32_t *length, uint32_t timeout);

  /// Check response using the ymodem protocol
  int32_t CheckResponse(uint8_t c);

  /// Prepare the first block
  void PrepareIntialPacket(uint8_t *data, const uint8_t* fileName, uint32_t *length);

  /// Prepare the data packet
  void PreparePacket(uint8_t *SourceBuf, uint8_t *data, uint8_t pktNo, uint32_t sizeBlk);
  
  /// Update CRC16 for input byte
  uint16_t UpdateCRC16(uint16_t crcIn, uint8_t byte);

  /// Calculate the CCRC16 for ymodem packet
  uint16_t Cal_CRC16(const uint8_t* data, uint32_t size);
  
  /// Calculate the checksum for YModem Packet
  uint8_t CalChecksum(const uint8_t* data, uint32_t size);
  
  /// Calculate the number of pages
  int8_t Erase(uint32_t location, uint32_t size);

  /// Convert a decismal or hexadecimal string to integer
  uint32_t Str2Int(uint8_t *inputstr, int32_t *intnum);
  
  /// Convert a integer to a C-string
  void Int2Str(uint8_t* str, int32_t intnum);
  
};  // end of class C_YMODEM




#endif  // C_YMODEM_H

