/** \defgroup C_YMODEM C_YMODEM_PackageTitle
 *
 * \brief This is the implementation of the Y-Modem transfer protocol
 */

#include <stdint.h>
#include <string.h>

#include "c_ymodem/c_ymodem.hpp"
#include "memory_definitions/memory_definitions.h"
#ifdef V0
  #include "v0/c_interface.hpp"
#endif

extern C_INTERFACE interface;

C_YMODEM::C_YMODEM(C_USART *usart)
{
  myUsart = usart;
  return;
}

C_YMODEM::~C_YMODEM()
{
  return;
}

void C_YMODEM::Handler(void)
{
  return;
}


int32_t C_YMODEM::ReceiveByte(uint8_t *c, uint32_t timeout)
{
  int32_t result = -1;
  
  myTimer.Start(1000);
  while ((C_TIMER::STATE_RUNNING == myTimer.GetState()) && (-1 == result))
  {
    interface.Handler();
    if (1 == myUsart->Read(c, 1))
    {
      result = 0;
    }
  }
  myTimer.Stop();
  return result;
}

uint32_t C_YMODEM::SendByte(uint8_t listen, uint8_t c)
{
  if (1 == listen) {
    ReceiveByte(&c, 1000);
    // don't send but listen for one byte
  }
  else {
    myUsart->Write(&c, 1);
  }
  return 0;
}

int32_t C_YMODEM::ReceivePacket (uint8_t *data, int32_t *length, uint32_t timeout)
{
  uint16_t i, packet_size;
  uint8_t c;
  *length = 0;
  if (ReceiveByte(&c, timeout) != 0)
  {
    return -1;
  }
  switch (c)
  {
    case SOH:
      packet_size = PACKET_SIZE_NORMAL;
      break;
    case STX:
      packet_size = PACKET_SIZE_1K;
      break;
    case EOT:
      return 0;
    case CA:
      if ((ReceiveByte(&c, timeout) == 0) && (c == CA))
      {
        *length = -1;
        return 0;
      }
      else
      {
        return -1;
      }
    case ABORT1:
    case ABORT2:
      return 1;
    default:
      return -1;
  }
  *data = c;
  for (i = 1; i < (packet_size + PACKET_OVERHEAD); i ++)
  {
    if (ReceiveByte(data + i, timeout) != 0)
    {
      return -1;
    }
  }
  if (data[PACKET_SEQNO_INDEX] != ((data[PACKET_SEQNO_COMP_INDEX] ^ 0xff) & 0xff))
  {
    return -1;
  }
  *length = packet_size;
  return 0;
}

int32_t C_YMODEM::ReceiveBlocking(uint8_t listen)
{
  uint8_t packetData[PACKET_SIZE_1K + PACKET_OVERHEAD];
  uint8_t file_size[FILE_SIZE_LENGTH];
  uint8_t *file_ptr;
  int32_t i;
  int32_t packetLength;
  int32_t session_done;
  int32_t file_done;
  int32_t packetsReceived;
  int32_t errors;
  int32_t session_begin;
  int32_t size = 0;

  /* Initialize FlashDestination variable */
  myAppMirrorDestination = APP_MIRROR;

  for (session_done = 0, errors = 0, session_begin = 0; ;)
  {
    for (packetsReceived = 0, file_done = 0; ;)
    {
      switch (ReceivePacket(packetData, &packetLength, NAK_TIMEOUT))
      {
        case 0:
          errors = 0;
          switch (packetLength)
          {
            /* Abort by sender */
            case - 1:
              SendByte(listen, ACK);
              return 0;
            /* End of transmission */
            case 0:
              SendByte(listen, ACK);
              file_done = 1;
              break;
            /* Normal packet */
            default:
              if ((packetData[PACKET_SEQNO_INDEX] & 0xff) != (packetsReceived & 0xff))
              {
                SendByte(listen, NAK);
              }
              else
              {
                if (packetsReceived == 0)
                {
                  /* Filename packet */
                  if (packetData[PACKET_HEADER] != 0)
                  {
                    /* Filename packet has valid data */
                    for (i = 0, file_ptr = packetData + PACKET_HEADER; (*file_ptr != 0) && (i < FILE_NAME_LENGTH);)
                    {
                      file_ptr++;
                    }
                    for (i = 0, file_ptr ++; (*file_ptr != ' ') && (i < FILE_SIZE_LENGTH);)
                    {
                      file_size[i++] = *file_ptr++;
                    }
                    file_size[i++] = '\0';
                    Str2Int(file_size, &size);

                    /* Test the size of the image to be sent */
                    /* Image size is greater than Flash size */
                    if (size > (APP_SIZE - 1))
                    {
                      /* End session */
                      SendByte(listen, CA);
                      SendByte(listen, CA);
                      return -1;
                    }
                    SendByte(listen, ACK);
                    SendByte(listen, CRC16);
                  }
                  /* Filename packet is empty, end session */
                  else
                  {
                    SendByte(listen, ACK);
                    file_done = 1;
                    session_done = 1;
                    break;
                  }
                }
                /* Data packet */
                else
                {
                  SendByte(listen, ACK);
                }
                packetsReceived ++;
                session_begin = 1;
              }
          }
          break;
        case 1:
          SendByte(listen, CA);
          SendByte(listen, CA);
          return -3;
        default:
          if (session_begin > 0)
          {
            errors++;
          }
          if (errors > MAX_ERRORS)
          {
            SendByte(listen, CA);
            SendByte(listen, CA);
            return 0;
          }
          SendByte(listen, CRC16);     // Aktuell
          break;
      }
      if (file_done != 0)
      {
        break;
      }
    }
    if (session_done != 0)
    {
      break;
    }
  }
  return (int32_t)size;
}

int32_t C_YMODEM::CheckResponse(uint8_t c)
{
  return 0;
}

void C_YMODEM::PrepareIntialPacket(uint8_t *data, const uint8_t* fileName, uint32_t *length)
{
  uint16_t i, j;
  uint8_t file_ptr[10];
  
  /* Make first three packet */
  data[0] = SOH;
  data[1] = 0x00;
  data[2] = 0xff;
  
  /* Filename packet has valid data */
  for (i = 0; (fileName[i] != '\0') && (i < FILE_NAME_LENGTH);i++)
  {
     data[i + PACKET_HEADER] = fileName[i];
  }

  data[i + PACKET_HEADER] = 0x00;
  
  Int2Str (file_ptr, *length);
  for (j =0, i = i + PACKET_HEADER + 1; file_ptr[j] != '\0' ; )
  {
     data[i++] = file_ptr[j++];
  }
  
  for (j = i; j < PACKET_SIZE_NORMAL + PACKET_HEADER; j++)
  {
    data[j] = 0;
  }
}

void C_YMODEM::PreparePacket(uint8_t *SourceBuf, uint8_t *data, uint8_t pktNo, uint32_t sizeBlk)
{
  uint16_t i, size, packetSize;
  uint8_t* file_ptr;
  
  /* Make first three packet */
  packetSize = sizeBlk >= PACKET_SIZE_1K ? PACKET_SIZE_1K : PACKET_SIZE_NORMAL;
  size = sizeBlk < packetSize ? sizeBlk :packetSize;
  if (packetSize == PACKET_SIZE_1K)
  {
     data[0] = STX;
  }
  else
  {
     data[0] = SOH;
  }
  data[1] = pktNo;
  data[2] = (~pktNo);
  file_ptr = SourceBuf;
  
  /* Filename packet has valid data */
  for (i = PACKET_HEADER; i < size + PACKET_HEADER;i++)
  {
     data[i] = *file_ptr++;
  }
  if ( size  <= packetSize)
  {
    for (i = size + PACKET_HEADER; i < packetSize + PACKET_HEADER; i++)
    {
      data[i] = 0x1A; /* EOF (0x1A) or 0x00 */
    }
  }
}

uint16_t C_YMODEM::UpdateCRC16(uint16_t crcIn, uint8_t byte)
{
 uint32_t crc = crcIn;
 uint32_t in = byte|0x100;
 do
 {
 crc <<= 1;
 in <<= 1;
 if(in&0x100)
 ++crc;
 if(crc&0x10000)
 crc ^= 0x1021;
 }
 while(!(in&0x10000));
 return crc&0xffffu;
}

uint16_t C_YMODEM::Cal_CRC16(const uint8_t* data, uint32_t size)
{
 uint32_t crc = 0;
 const uint8_t* dataEnd = data+size;
 while(data<dataEnd)
  crc = UpdateCRC16(crc,*data++);
 
 crc = UpdateCRC16(crc,0);
 crc = UpdateCRC16(crc,0);
 return crc&0xffffu;
}

uint8_t C_YMODEM::CalChecksum(const uint8_t* data, uint32_t size)
{
 uint32_t sum = 0;
 const uint8_t* dataEnd = data+size;
 while(data < dataEnd )
   sum += *data++;
 return sum&0xffu;
}

int8_t C_YMODEM::Erase(uint32_t location, uint32_t size)
{
  int8_t result = 0;
  FLASH_Status flashState = FLASH_COMPLETE;
  uint32_t pagenumber = 0x0;

  if ((size % PAGE_SIZE) != 0)
  {
    pagenumber = (size / PAGE_SIZE) + 1;
  }
  else
  {
    pagenumber = size / PAGE_SIZE;
  }

  /* Erase the FLASH pages */
  for (int EraseCounter = 0; (EraseCounter < pagenumber) && (flashState == FLASH_COMPLETE); EraseCounter++)
  {
    flashState = FLASH_ErasePage(location + (PAGE_SIZE * EraseCounter));
  }
  
  if (FLASH_COMPLETE != flashState) {
    result = -1;
  }

  return result;
}

uint32_t C_YMODEM::Str2Int(uint8_t *inputstr, int32_t *intnum)
{
  uint32_t i = 0, res = 0;
  uint32_t val = 0;

  if (inputstr[0] == '0' && (inputstr[1] == 'x' || inputstr[1] == 'X'))
  {
    if (inputstr[2] == '\0')
    {
      return 0;
    }
    for (i = 2; i < 11; i++)
    {
      if (inputstr[i] == '\0')
      {
        *intnum = val;
        /* return 1; */
        res = 1;
        break;
      }
      if (ISVALIDHEX(inputstr[i]))
      {
        val = (val << 4) + CONVERTHEX(inputstr[i]);
      }
      else
      {
        /* return 0, Invalid input */
        res = 0;
        break;
      }
    }
    /* over 8 digit hex --invalid */
    if (i >= 11)
    {
      res = 0;
    }
  }
  else /* max 10-digit decimal input */
  {
    for (i = 0;i < 11;i++)
    {
      if (inputstr[i] == '\0')
      {
        *intnum = val;
        /* return 1 */
        res = 1;
        break;
      }
      else if ((inputstr[i] == 'k' || inputstr[i] == 'K') && (i > 0))
      {
        val = val << 10;
        *intnum = val;
        res = 1;
        break;
      }
      else if ((inputstr[i] == 'm' || inputstr[i] == 'M') && (i > 0))
      {
        val = val << 20;
        *intnum = val;
        res = 1;
        break;
      }
      else if (ISVALIDDEC(inputstr[i]))
      {
        val = val * 10 + CONVERTDEC(inputstr[i]);
      }
      else
      {
        /* return 0, Invalid input */
        res = 0;
        break;
      }
    }
    /* Over 10 digit decimal --invalid */
    if (i >= 11)
    {
      res = 0;
    }
  }

  return res;
}

void C_YMODEM::Int2Str(uint8_t* str, int32_t intnum)
{
  uint32_t i, Div = 1000000000, j = 0, Status = 0;

  for (i = 0; i < 10; i++)
  {
    str[j++] = (intnum / Div) + 48;

    intnum = intnum % Div;
    Div /= 10;
    if ((str[j-1] == '0') & (Status == 0))
    {
      j = 0;
    }
    else
    {
      Status++;
    }
  }
}

