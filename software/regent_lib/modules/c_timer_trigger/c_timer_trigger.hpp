/**
 * \class C_TIMER_TRIGGER
 *
 * \ingroup C_TIMER_TRIGGER
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_TIMER_TRIGGER implementation
 *
 * This class implements the trigger class to trigger
 * all timer instances.
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_TIMER_TRIGGER_H
#define C_TIMER_TRIGGER_H

#include <stdint.h>

#include "c_timer/c_timer.hpp"
#include "c_timer_element/c_timer_element.hpp"

#define MAX_TIMERS   30

class C_TIMER_TRIGGER
{

private:

  C_TIMER   *myTimers[MAX_TIMERS];

public:

  /// Create an C_TIMER_TRIGGER
  C_TIMER_TRIGGER();

  /// Delete an C_TIMER_TRIGGER
  ~C_TIMER_TRIGGER();

  /// Add another timer instance for triggering
  void AddTimer(C_TIMER *timer);

  /// Remove a timer instance from triggering
  void RemoveTimer(C_TIMER *timer);

  /// Trigger the timers
  void Trigger(uint16_t milliseconds);

private:

  /// Find a specific timer
  int8_t FindTimer(C_TIMER *timer);

};  // end of class C_TIMER_TRIGGER

#endif  // C_TIMER_TRIGGER_H

