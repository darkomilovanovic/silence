/** \defgroup C_TIMER_TRIGGER C_TIMER_TRIGGER_PackageTitle
 *
 * \brief This is the implementation of the timer trigger class
 */

#include "c_timer_trigger/c_timer_trigger.hpp"


C_TIMER_TRIGGER::C_TIMER_TRIGGER()
{
  for (int i=0; i<MAX_TIMERS; i++) {
    myTimers[i] = NULL;
  }
  return;
}

C_TIMER_TRIGGER::~C_TIMER_TRIGGER()
{
  return;
}

void C_TIMER_TRIGGER::AddTimer(C_TIMER *timer)
{
  int8_t index;
  
  index = FindTimer(timer);
  if (0 > index) {
    for (index=0; index<MAX_TIMERS; index++) {
      if (NULL == myTimers[index]) {
        break;
      }
    }
  }
  
  if (MAX_TIMERS > index) {
    myTimers[index] = timer;
  }
  
  return;
}

int8_t C_TIMER_TRIGGER::FindTimer(C_TIMER *timer)
{
  int8_t i;
  
  for (i=0; i<MAX_TIMERS; i++) {
    if (timer == myTimers[i]) {
      break;
    }
  }
  
  if (i == MAX_TIMERS) {
    i = -1;
  }
  
  return i;
}

void C_TIMER_TRIGGER::RemoveTimer(C_TIMER *timer)
{
  int8_t index;
  
  index = FindTimer(timer);
  if (0 <= index) {
    myTimers[index] = NULL;
  }

  return;
}

void C_TIMER_TRIGGER::Trigger(uint16_t milliseconds) {
  
  for (int i=0; i<MAX_TIMERS; i++) {
    if (NULL != myTimers[i]) {
      myTimers[i]->Trigger(milliseconds);
    }
  }

  return;
}
