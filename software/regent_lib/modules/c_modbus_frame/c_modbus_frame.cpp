/** \defgroup C_MODBUS_FRAME C_MODBUS_FRAME_PackageTitle
 *
 * \brief This is the implementation of the buffer class
 */

#include <stdint.h>
#include <string.h>

#include "c_modbus_frame/c_modbus_frame.hpp"

C_MODBUS_FRAME::C_MODBUS_FRAME() : C_BUFFER()
{
  return;
}

uint8_t C_MODBUS_FRAME::Verify(void)
{
  uint16_t crc = 0xFFFF;
  uint16_t crcFrame = 0;
  uint8_t result = 1;

  crc = CalcCRC(myUsedBufferSpace-2);

  crcFrame = myBuffer[myUsedBufferSpace-2];
  crcFrame += ((uint16_t)myBuffer[myUsedBufferSpace-1]) << 8;

  if (crc == crcFrame) result = 0;
  
  return result;
}

uint16_t C_MODBUS_FRAME::CalcCRC(uint16_t dataLength)
{
  uint16_t crc = 0xFFFF;

  for (uint16_t i = 0; i < dataLength; i++)
    crc = (uint16_t)(crcTable[(crc ^ ((uint16_t)(myBuffer[i]))) & 0x00ff] ^ (crc >> 8));

  return crc;
}

uint8_t C_MODBUS_FRAME::GetAddress(void)
{
  return myBuffer[0];
}
void C_MODBUS_FRAME::SetSlaveAddress(uint8_t Address)
{
  myBuffer[0] = Address;
  myWritePosition = myBuffer + 1;
  myReadPosition = myBuffer;
  myUsedBufferSpace = 1;
}
void C_MODBUS_FRAME::SetFunctionCode(MODBUS_FUNCTION_CODE code)
{
  myBuffer[1] = code;
}
void C_MODBUS_FRAME::SetSlaveFunctionCode(MODBUS_FUNCTION_CODE code)
{
  myBuffer[1] = code;
  myWritePosition = myBuffer + 2;
  myReadPosition = myBuffer;
  myUsedBufferSpace += 1;
}

void C_MODBUS_FRAME::SetErrorCode(MODBUS_ERROR_CODE error)
{
  myBuffer[1] |= 0x80;
  myBuffer[2] = error;
  myWritePosition = myBuffer + 3;
  myReadPosition = myBuffer;
  myUsedBufferSpace = 3;
}

void C_MODBUS_FRAME::GenerateCRC(void)
{
  uint16_t crc = CalcCRC(myUsedBufferSpace);
  uint8_t *pCrc = (uint8_t *)&crc;
  WriteU8(pCrc, 1);
  WriteU8(pCrc+1, 1);
}

uint8_t C_MODBUS_FRAME::GetFunctionCode(void)
{
  return myBuffer[1];
}

void C_MODBUS_FRAME::SetCount(uint8_t len)
{
  myUsedBufferSpace = len;
  myWritePosition = &myBuffer[len];
  myReadPosition = myBuffer;
}

uint16_t C_MODBUS_FRAME::GetCount(void)
{
  return myUsedBufferSpace;
}

uint16_t C_MODBUS_FRAME::GetHRStartAddress(void)
{
  uint16_t address = 0;
  
  address = myBuffer[2] << 8;
  address |= myBuffer[3];
  
  myReadPosition = &myBuffer[4];
  
  return address;
}
uint16_t C_MODBUS_FRAME::GetSlaveHRStartAddress(void)
{
  uint16_t HRaddress = 0;
  
  HRaddress = myBuffer[2] << 8;
  HRaddress |= myBuffer[3];
  
  myReadPosition = &myBuffer[4];
  
  return HRaddress;
}
uint16_t C_MODBUS_FRAME::GetSlaveHRValue(void)
{
  uint16_t HRvalue = 0;
  
  HRvalue = myBuffer[3] << 8;
  HRvalue |= myBuffer[4];
  
  myReadPosition = &myBuffer[5];
  
  return HRvalue;
}
uint8_t C_MODBUS_FRAME::GetSlaveAddress(void)
{
  return myBuffer[0];
}
uint8_t C_MODBUS_FRAME::GetSlaveHRQuantityReadHR(void)
{
  return myBuffer[2];
}

uint16_t C_MODBUS_FRAME::GetSlaveHRQuantityWriteHR(void)
{
 uint16_t quantity = 0;
  
  quantity = myBuffer[4] << 8;
  quantity |= myBuffer[5];
  
  myReadPosition = &myBuffer[6];
  
  return quantity;
}

uint8_t C_MODBUS_FRAME::GetSlaveFunction(void)
{
  return myBuffer[1];
}
uint16_t C_MODBUS_FRAME::GetHRQuantity(void)
{
  uint16_t quantity = 0;
  
  quantity = myBuffer[4] << 8;
  quantity |= myBuffer[5];
  
  myReadPosition = &myBuffer[6];
  
  return quantity;
}

uint8_t C_MODBUS_FRAME::GetByteQuantity(void)
{
  myReadPosition = &myBuffer[7];
  
  return myBuffer[6];
}

void C_MODBUS_FRAME::SetByteQuantity(uint8_t quantity)
{
  myBuffer[2] = quantity;
  myWritePosition = myBuffer + 3;
  myReadPosition = myBuffer;
  myUsedBufferSpace = 3;
}
