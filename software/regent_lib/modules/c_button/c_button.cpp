/** \defgroup C_BUTTON C_BUTTON_PackageTitle
 *
 * \brief This is the implementation of the button class
 */

#include "c_button/c_button.hpp"

C_BUTTON::C_BUTTON(C_INPUT *input, C_BUTTON_POLARITY polarity, uint16_t shortTimeMin, uint16_t shortTimeMax, uint16_t longTimeMin, uint16_t longTimeMax)
{
  myInput = input;
  myPolarity = polarity;
  myEvent = BUTTON_NO_EVENT;
  myShortPressTimeMin = shortTimeMin;
  myShortPressTimeMax = shortTimeMax;
  myLongPressTimeMin = longTimeMin;
  myLongPressTimeMax = longTimeMax;
  myButtonState = BUTTON_ENABLED;
  myFilterTimer.SetDuration(MY_INPUT_FILTER_TIME);
  myFilterTimer.Start();
}

C_BUTTON_EVENT C_BUTTON::GetEvent(void)
{
  return myEvent;
}

C_BUTTON_STATE C_BUTTON::GetState(void)
{
  C_BUTTON_STATE state = BUTTON_RELEASED;
  uint8_t activeState = 1;
  
  if (BUTTON_POLARITY_INVERTED == myPolarity) {
    activeState = 0;
  }

  if (activeState == myInput->Get()) 
  {
    state = BUTTON_PRESSED;
  }
  
  return state;
}

void C_BUTTON::SetPolarity(C_BUTTON_POLARITY polarity)
{
  myPolarity = polarity;
}
void C_BUTTON::Enable(void)
{
  myButtonState = BUTTON_ENABLED;
}

void C_BUTTON::Disable(void)
{
  myButtonState = BUTTON_DISABLED;
}

void C_BUTTON::Restart(uint16_t shortTimeMin, uint16_t shortTimeMax, uint16_t longTimeMin, uint16_t longTimeMax)
{
  myShortPressTimeMin = shortTimeMin;
  myShortPressTimeMax = shortTimeMax;
  myLongPressTimeMin = longTimeMin;
  myLongPressTimeMax = longTimeMax;
  myTime = 0;
  
}
uint16_t C_BUTTON::GetNumberOfShortPresses(void)
{
  return myNumberOfShortPresses;
}

uint16_t C_BUTTON::GetNumberOfLongPresses(void)
{
  return myNumberOfLongPresses;
}
void C_BUTTON::Handler()
{
  if(myFilterTimer.GetState() == C_TIMER::STATE_OVER && myButtonState == BUTTON_ENABLED)
  {
    if(GetState() == BUTTON_PRESSED)
    {
      myTime += MY_INPUT_FILTER_TIME;
      if(myTime >= myShortPressTimeMin && myTime <= myShortPressTimeMax)
       myEvent = BUTTON_SINGLE_CLICK;  
      if(myTime >= myLongPressTimeMin && myTime <= myLongPressTimeMax)
       myEvent = BUTTON_LONG_CLICK;
      if(myTime > myLongPressTimeMax && myLongPressTimeMax != 0xFFFF)
        myEvent = BUTTON_DEFECT;
    }
    else
    {
      if(myTime >= myShortPressTimeMin && myTime <= myShortPressTimeMax)
       myNumberOfShortPresses += 1;  
      if(myTime >= myLongPressTimeMin && myTime <= myLongPressTimeMax)
       myNumberOfLongPresses += 1;
      myTime = 0;
      myEvent = BUTTON_NO_EVENT;  
    }
    myFilterTimer.Restart();
  }
}
