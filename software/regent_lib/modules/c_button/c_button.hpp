/**
 * \class C_BUTTON
 *
 * \ingroup C_BUTTON
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_BUTTON implementation
 *
 * This class implements a button with a kind of states
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_BUTTON_H
#define C_BUTTON_H

#define MY_INPUT_FILTER_TIME 60

#include "c_input/c_input.hpp"
#include "c_module/c_module.hpp"
#include "c_timer/c_timer.hpp"

enum C_BUTTON_EVENT { 
  BUTTON_NO_EVENT = 0x0000,
  BUTTON_SINGLE_CLICK = 0x0001,
  BUTTON_LONG_CLICK = 0x10,
  BUTTON_DOUBLE_CLICK = 0x0100,
  BUTTON_DEFECT = 0x1000
};

enum C_BUTTON_STATE {
  BUTTON_PRESSED = 0,
  BUTTON_RELEASED = 1, 
  BUTTON_ENABLED = 2,
  BUTTON_DISABLED = 3
};

enum C_BUTTON_POLARITY {
  BUTTON_POLARITY_NORMAL,
  BUTTON_POLARITY_INVERTED
};

class C_BUTTON : public C_MODULE
{

private:
  C_INPUT           *myInput;
  C_TIMER           myFilterTimer;
  C_BUTTON_EVENT    myEvent;
  C_BUTTON_POLARITY myPolarity;
  uint16_t          myShortPressTimeMin;
  uint16_t          myShortPressTimeMax;
  uint16_t          myLongPressTimeMin;
  uint16_t          myLongPressTimeMax;
  uint16_t          myNumberOfShortPresses;
  uint16_t          myNumberOfLongPresses;
  uint16_t          myNumberOfDoublePresses;
  uint16_t          myTime;
  C_BUTTON_STATE    myButtonState;


public:

  /// Create an C_BUTTON
  C_BUTTON(C_INPUT *input, C_BUTTON_POLARITY polarity, uint16_t shortTimeMin, uint16_t shortTimeMax, uint16_t longTimeMin, uint16_t longTimeMax);


  /// Delete an C_BUTTON
  ~C_BUTTON();

  /// Receive the last event of the button
  C_BUTTON_EVENT GetEvent();

  /// Receive the actual state of the button
  C_BUTTON_STATE GetState();

  uint16_t GetNumberOfShortPresses();
 
  uint16_t GetNumberOfLongPresses();

  void Enable();

  void Disable();
  /// Set the polarity of the button
  void SetPolarity(C_BUTTON_POLARITY polarity);

  void Restart(uint16_t shortTimeMin, uint16_t shortTimeMax, uint16_t longTimeMin, uint16_t longTimeMax);
  
  virtual void Handler();

};  // end of class C_BUTTON

#endif  // C_BUTTON_H

