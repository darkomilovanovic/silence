/**
 * \class C_BUFFER
 *
 * \ingroup C_BUFFER
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_BUFFER implementation
 *
 * This class implements a buffer to write to and read from
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_BUFFER_H
#define C_BUFFER_H

#define BUFFER_SIZE 256

#include <stdint.h>
#include "c_module/c_module.hpp"

class C_BUFFER : public C_MODULE
{

protected:
  uint8_t     myBuffer[BUFFER_SIZE];
  uint8_t     *myWritePosition;
  uint8_t     *myReadPosition;
  uint16_t    myUsedBufferSpace;

public:

  /// Create an C_BUFFER
  C_BUFFER();

  /// Delete an C_BUFFER
  ~C_BUFFER();

  /// Operation
  C_BUFFER& operator = (const C_BUFFER& src);

  /// Write to the buffer
  uint16_t WriteU8(uint8_t *data, uint16_t length);

  uint16_t WriteU16(uint16_t data);

  uint16_t WriteU32(uint32_t data);

  /// Read from the buffer
  uint16_t Read(uint8_t *data, uint16_t length);

  /// Get the total length of the buffer
  uint16_t GetLength();

  /// Get the actual filled buffer size
  uint16_t Count();
  
  /// Reset the buffer
  void Reset();
  
  /// Get data pointer
  uint8_t* GetData();
  
  /// Copy a buffer to another
  void CopyFrom(C_BUFFER *src);

};  // end of class C_BUFFER

#endif  // C_BUFFER_H

