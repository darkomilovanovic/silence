/** \defgroup C_BUFFER C_BUFFER_PackageTitle
 *
 * \brief This is the implementation of the buffer class
 */

#include <stdint.h>
#include <string.h>

#include "c_buffer/c_buffer.hpp"

C_BUFFER::C_BUFFER()
{
  memset(myBuffer, 0, sizeof(myBuffer));
  myWritePosition = myBuffer;
  myReadPosition = myBuffer;
  myUsedBufferSpace = 0;
  return;
}

C_BUFFER& C_BUFFER::operator = (const C_BUFFER& src)
{
  memcpy(myBuffer, src.myBuffer, sizeof(myBuffer));
  myWritePosition = myBuffer + (src.myWritePosition - src.myBuffer);
  myReadPosition = myBuffer + (src.myReadPosition - src.myBuffer);
  myUsedBufferSpace = src.myUsedBufferSpace;
  return *this;  
}

uint16_t C_BUFFER::WriteU8(uint8_t *data, uint16_t length)
{
  uint16_t result = 0;

  if (length <= (sizeof(myBuffer) - myUsedBufferSpace)) {
    for (int i=0; i<length; i++) 
    {
      *myWritePosition = *data;
      data += 1;
      myWritePosition += 1;
      myUsedBufferSpace += 1;
	  if (myWritePosition >= (myBuffer + sizeof(myBuffer))) {
        myWritePosition = myBuffer;
}
    }
    result = length;
  }
  return result;
}

uint16_t C_BUFFER::WriteU16(uint16_t data)
{
  uint16_t result = 0;
  uint8_t *pData = (uint8_t*)&data;
  if (2 <= (sizeof(myBuffer) - myUsedBufferSpace)) 
  {
    WriteU8(pData+1 , 1);
    WriteU8(pData , 1);    
    result = 2;
  }
  return result;
}

uint16_t C_BUFFER::WriteU32(uint32_t data)
{
  uint16_t result = 0;
  uint8_t *pData = (uint8_t*)&data;
  if (4 <= (sizeof(myBuffer) - myUsedBufferSpace)) 
  {
    WriteU8(pData+3 , 1);
    WriteU8(pData+2 , 1);
    WriteU8(pData+1 , 1);
    WriteU8(pData+0 , 1);    
    result = 4;
  }
  return result;
}
 
uint16_t C_BUFFER::Read(uint8_t *data, uint16_t length)
{
  uint16_t i = 0;
  
  if ( (NULL != data) && (0 < length)) {
    for (i=0; (i<length) && (0<myUsedBufferSpace) && (myReadPosition != myWritePosition); i++) {
      data[i] = *myReadPosition;
      myUsedBufferSpace--;
      myReadPosition++;
      if (myReadPosition >= (myBuffer + sizeof(myBuffer))) {
        myReadPosition = myBuffer;
      }
    }
  }
  
  return i;
}

uint16_t C_BUFFER::GetLength(void)
{
  return sizeof(myBuffer);
}

uint16_t C_BUFFER::Count(void)
{
  return myUsedBufferSpace;
}

void C_BUFFER::Reset()
{
  memset(myBuffer, 0, sizeof(myBuffer));
  myWritePosition = myBuffer;
  myReadPosition = myBuffer;
  myUsedBufferSpace = 0;
  return;
}

uint8_t* C_BUFFER::GetData()
{
  return myBuffer;
}

void C_BUFFER::CopyFrom(C_BUFFER *src)
{
  memcpy(myBuffer, src->myBuffer, sizeof(myBuffer));
  myWritePosition = myBuffer + (src->myWritePosition - src->myBuffer);
  myReadPosition = myBuffer + (src->myReadPosition - src->myBuffer);
  myUsedBufferSpace = src->myUsedBufferSpace;
  return;
}
