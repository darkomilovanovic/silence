/**
 * \class C_ADC
 *
 * \ingroup C_ADC
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_ADC implementation
 *
 * This class implements an adc for the GPIO for the STM32F0xx processor.
 * An input represents a single pin on the processor.
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_ADC_H
#define C_ADC_H

#include <stdint.h>
#include <stddef.h>
#include "c_timer/c_timer.hpp"


enum UNIT{
  NO_UNIT = 0,
  VOLTAGE = 1,
  VOLTAGE_VDDA = 2,
  CURRENT = 3,
  TEMPERATURE = 4
};
class C_ADC
{

public:
 
  /// Create an C_ADC
  C_ADC(uint16_t const * DMAArray, uint8_t position, UNIT unitValue);
  
  /// Delete an C_ADC
  ~C_ADC();

  uint16_t     GetValue(void);
  uint16_t     GetValueFiltered(void);
  virtual void Handler(void);
  
private:
  
  C_TIMER      myTimer;
  uint8_t      myPosition;
  uint16_t const * myMeasuredValue;
  uint16_t     myFilteredValue;
  uint16_t     myLastMeasuredValue;
  uint16_t     myMeasuredConvertedValue;
  uint32_t     myIntegrator;
  UNIT         myUnit;
};  // end of class C_ADC

#endif  // C_ADC_H

