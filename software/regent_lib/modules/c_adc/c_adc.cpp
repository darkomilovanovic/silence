/** \defgroup C_OUTPUT C_OUTPUT
 *
 * \brief Provide some stuff to do stuff
 */

#include "c_adc/c_adc.hpp"

#define TEMP110_CAL_ADDR ((uint16_t*)((uint32_t) 0x1FFFF7C2))
#define TEMP30_CAL_ADDR ((uint16_t*)((uint32_t) 0x1FFFF7B8))
#define VREFINT_CAL ((uint16_t*)((uint32_t) 0x1FFFF7BA))
#define VDD_CALIB ((uint16_t) (330))
#define VDD_APPLI ((uint16_t)(300))

C_ADC::C_ADC(uint16_t const * DMAArray, uint8_t position, UNIT unitValue)
{
  myMeasuredValue = DMAArray + position;
  myLastMeasuredValue = 0;
  myFilteredValue = 0; 
  myIntegrator = 0;
  myPosition = position;
  myUnit = unitValue;
}

uint16_t C_ADC::GetValue()
{
  if( myUnit == TEMPERATURE)
  {
    int temperature = 0; // temp in Grad Celsius
    temperature = ( ((int32_t) ((*myMeasuredValue) * VDD_APPLI / VDD_CALIB))-(int32_t) *TEMP30_CAL_ADDR );
    temperature = temperature * (int32_t)(110-30);
    temperature = temperature / (int32_t)(*TEMP110_CAL_ADDR - *TEMP30_CAL_ADDR);
    temperature = temperature + 30; //A.7.16 temperature + 30;
    myMeasuredConvertedValue = (uint16_t)temperature;
  }
  else if(myUnit == VOLTAGE)
  {
    uint16_t millivolt = 0;
    uint16_t volt = 0;
    volt=(((*myMeasuredValue) * 3300) / 0xFFF) / 1000;
    millivolt = ((((*myMeasuredValue) * 3300) / 0xFFF)%1000);
    myMeasuredConvertedValue = millivolt + (1000 * volt);
  }
  else if(myUnit == VOLTAGE_VDDA)   
  {
    uint32_t vdda = 0;
    
    vdda = 3300 * (uint32_t)(*VREFINT_CAL) / (uint32_t)(*myMeasuredValue);
    myMeasuredConvertedValue = (uint16_t)vdda;
  }
  else if(myUnit == CURRENT)
  {
    uint32_t milliampere = 0;
    milliampere = *myMeasuredValue * 200 / 4096;
    myMeasuredConvertedValue = (uint16_t)milliampere;
  }
  else if(myUnit == NO_UNIT)
    myMeasuredConvertedValue = *myMeasuredValue;
  else
    myMeasuredConvertedValue = 0;
  return myMeasuredConvertedValue;
}

uint16_t C_ADC::GetValueFiltered(void)
{
  return myFilteredValue;
}

void C_ADC::Handler(void) //wird jede [ms] aufgerufen
{ 
//  if(myTimer.GetState() == C_TIMER::STATE_OVER)
//  {
//    //Hochpass 1. Ordnung, f0 = 1/2*PI*T*2^K -> 0.012Hz = 1/2*pi*100ms*2^7 (K = 7)
//    #define HIGHPASS_K     7
//    #define HIGHPASS_SHIFT 8
//    
//    uint32_t difference = 0;
//    myLastMeasuredValue = *myMeasuredValue;
//    
//    if (myUnit != VOLTAGE_VBAT)
//      difference = (*myMeasuredValue << HIGHPASS_SHIFT) - myIntegrator;
//    else
//      difference = (((*myMeasuredValue) * 2) << HIGHPASS_SHIFT) - myIntegrator;
//    myIntegrator = myIntegrator + (difference >> HIGHPASS_K);  
//    myFilteredValue = (int)(difference >> HIGHPASS_SHIFT); 
//    myTimer.Restart();
//  }  
}
