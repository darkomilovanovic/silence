/** \defgroup C_MODULE C_MODULE_PackageTitle
 *
 * \brief This is the implementation of the standard module
 */


#include "c_module/c_module.hpp"

C_MODULE::C_MODULE()
{
  return;
}

C_MODULE::~C_MODULE()
{
  return;
}

void C_MODULE::Handler(void)
{
  return;
}

void C_MODULE::Start(void)
{
  return;
}

void C_MODULE::Stop(void)
{
  return;
}
