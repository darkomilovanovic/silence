/**
 * \class C_MODULE
 *
 * \ingroup C_MODULE
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_MODULE implementation
 *
 * This class implements a the standard module behaviour and interface
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_MODULE_H
#define C_MODULE_H


class C_MODULE
{

private:

public:

  /// Create an C_MODULE
  C_MODULE();

  /// Delete an C_MODULE
  ~C_MODULE();

  /// Handler
  void Handler(void);

  void Start(void);

  void Stop(void);
};  // end of class C_MODULE

#endif  // C_MODULE_H

