/**
 * \class C_VERSION
 *
 * \ingroup C_VERSION
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_VERSION implementation
 *
 * This class implements the version handling
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_VERSION_H
#define C_VERSION_H

#include "c_module/c_module.hpp"

class C_VERSION : public C_MODULE
{

private:
  #include "version.vid"
  #include "git.vid"
 
#ifdef V0
   #include "v0/hw_version.vid"
#endif
  char    mySWVersion[64];
  char    mySWVersionBL[64];
  char    myHWVersion[16];

public:

  /// Create an C_VERSION
  C_VERSION();

  /// Delete an C_VERSION
  ~C_VERSION();

  /// Read the software version
  char* GetSWVersion(void);

  /// Read the software version
  char* GetHWVersion(void);

  char* GetSWVersionBL(void);

};  // end of class C_VERSION

#endif  // C_VERSION_H

