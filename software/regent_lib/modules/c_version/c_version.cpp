/** \defgroup C_VERSION C_VERSION_PackageTitle
 *
 * \brief This is the implementation of the version handling
 */

#include <stdio.h>

#include "c_version/c_version.hpp"
#include "memory_definitions/memory_definitions.h"

/* Maximum string size: 16byte, see address definition below. */
const char __attribute__((at(VERSION_ADDRESS))) firmwareProject[] = FIRMWARE_TYPE;
const char __attribute__((at(VERSION_ADDRESS + 0x10))) firmwareVersion[] = FIRMWARE_VERSION;

#if defined OFFICIAL_RELEASE
  const char __attribute__((at(VERSION_ADDRESS + 0x20))) firmwareBuild[]   = FIRMWARE_BUILD;
#else
  const char __attribute__((at(VERSION_ADDRESS + 0x20))) firmwareBuild[]   = "00";
#endif

#if defined RELEASE
  const char __attribute__((at(VERSION_ADDRESS + 0x40))) firmwareTarget[]  = "rel";
#elif defined DEBUG
  const char __attribute__((at(VERSION_ADDRESS + 0x40))) firmwareTarget[]  = "deb";
#else
  const char __attribute__((at(VERSION_ADDRESS + 0x40))) firmwareTarget[]  = "???";
#endif

const char __attribute__((at(VERSION_ADDRESS + 0x30))) gitCommit[]   = GIT_PATTERN;
const char __attribute__((at(VERSION_ADDRESS + 0x50))) hwTarget[] = HW_VERSION;

C_VERSION::C_VERSION()
{
  char * pToSWVersionBL;
  //char SWVersionBL[64];
  snprintf(mySWVersion, sizeof(mySWVersion), "%s-%s.%s-%s-%s",
    firmwareProject,
    firmwareVersion,
    firmwareBuild,
    gitCommit,
    firmwareTarget);
  
  snprintf(myHWVersion, sizeof(myHWVersion), "%s",
    hwTarget);
  
  pToSWVersionBL = (char*)VERSION_ADDRESS_BL;
  snprintf(mySWVersionBL, sizeof(mySWVersionBL), "%s-%s.%s-%s-%s", 
   pToSWVersionBL,
   (pToSWVersionBL+0x10),
   (pToSWVersionBL+0x20),
   (pToSWVersionBL+0x30),
   (pToSWVersionBL+0x40) );

  return;
}

C_VERSION::~C_VERSION()
{
  return;
}

char* C_VERSION::GetSWVersion(void)
{
  return mySWVersion;
}

char* C_VERSION::GetHWVersion(void)
{
  return myHWVersion;
}

char* C_VERSION::GetSWVersionBL(void)
{
  return mySWVersionBL;
}
