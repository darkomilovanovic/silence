/**
 * \class C_DEVICE_UPDATE
 *
 * \ingroup C_DEVICE_UPDATE
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_DEVICE_UPDATE implementation
 *
 * This class implements the device update mechanism
 *
 * \author Regent Lighting Ltd.
 *
 */
#ifndef __C_DEVICE_UPDATE_H__ 
#define __C_DEVICE_UPDATE_H__  

/** Class description.  
*/ 
class C_DEVICE_UPDATE { 
protected:
  typedef enum {
    DEVICE_UPDATE_STATE_INIT                = 0,
    DEVICE_UPDATE_STATE_ERASE_INIT,
    DEVICE_UPDATE_STATE_ERASE_UNLOCK_FLASH,
    DEVICE_UPDATE_STATE_ERASE_LOCK_FLASH,
    DEVICE_UPDATE_STATE_ERASE_FLASH,
    DEVICE_UPDATE_STATE_ERASE_FLASH_WAIT,
    DEVICE_UPDATE_STATE_WRITE_INIT,
    DEVICE_UPDATE_STATE_WRITE,
    DEVICE_UPDATE_STATE_WRITE_WAIT,
    DEVICE_UPDATE_STATE_CALC_CRC32,
    DEVICE_UPDATE_STATE_FLASH
  } t_device_update_erase_state;
  
  t_device_update_erase_state myState;
  uint32_t                    flash_address;
  uint32_t                    flash_address_crc32;

  uint32_t                    *p_data;
  uint8_t                     nr_of_uint32;
  uint8_t                     offset;
  uint32_t                    valid_crc32;
  uint32_t                    fw_crc32;
  bool                        finished;
  uint8_t                     error;
  
public: 

  C_DEVICE_UPDATE();
    
  void Handler();
  void Reset();

  void Erase();
  void Write(uint32_t *data, uint8_t nr_dwords, uint32_t crc32);
  void Update();
  void CalcCRC32();
    
  bool IsReady(uint8_t *error);

  uint32_t GetFirmwareCRC32();
    
protected:
  void Finish();

};/* C_DEVICE_UPDATE */ 


#endif 
