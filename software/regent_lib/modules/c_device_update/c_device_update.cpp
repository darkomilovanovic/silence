#include <stdint.h>
#include <stdio.h> 
#include <stdlib.h> 

/* Includes ------------------------------------------------------------------*/ 
#include "stm32f0xx.h"
#include "stm32f0xx_wwdg.h"

#include "memory_definitions\memory_definitions.h"
#include "c_device_update\c_device_update.hpp" 
#include "c_update/c_update.hpp"

/* Public function -----------------------------------------------------------*/ 

/**
*/
C_DEVICE_UPDATE::C_DEVICE_UPDATE()
{
  Reset();
  return;
}


/**
*/
void C_DEVICE_UPDATE::Reset()
{
  error               = 0;
  finished            = true;
  flash_address       = APP_MIRROR;
  offset              = 0;
  myState               = DEVICE_UPDATE_STATE_INIT;
}


/**
*/
void C_DEVICE_UPDATE::Update()
{
  myState = DEVICE_UPDATE_STATE_FLASH;
  finished = false;
  return;
}

/**
*/
void C_DEVICE_UPDATE::Erase() {
  myState = DEVICE_UPDATE_STATE_ERASE_INIT;
  finished = false;
  return;
}


/**
*/
void C_DEVICE_UPDATE::Write(uint32_t *data, uint8_t nr_dwords, uint32_t crc32)
{
  p_data = data;
  nr_of_uint32 = nr_dwords;
  valid_crc32 = crc32;
  myState = DEVICE_UPDATE_STATE_WRITE_INIT;
  finished = false;
  return;
}


/**
*/
void C_DEVICE_UPDATE::CalcCRC32()
{
  myState = DEVICE_UPDATE_STATE_CALC_CRC32;
  finished = false;
}


/**
*/
void C_DEVICE_UPDATE::Handler() {
  uint32_t crc32 = 0;
  C_UPDATE update;

  switch (myState) {
    case DEVICE_UPDATE_STATE_INIT:
    break;
    // erase
    case DEVICE_UPDATE_STATE_ERASE_INIT:
      flash_address = APP_MIRROR;
    case DEVICE_UPDATE_STATE_ERASE_UNLOCK_FLASH:
      FLASH_Unlock();
      myState = DEVICE_UPDATE_STATE_ERASE_FLASH;
      break;
    case DEVICE_UPDATE_STATE_ERASE_FLASH:
      FLASH_ErasePage((uint32_t)flash_address);
      myState = DEVICE_UPDATE_STATE_ERASE_FLASH_WAIT;
      break;
    case DEVICE_UPDATE_STATE_ERASE_FLASH_WAIT:
      if (FLASH_COMPLETE == FLASH_GetStatus()) {
        if ((APP_MIRROR + APP_SIZE) <= (uint32_t)flash_address) {
          flash_address = APP_MIRROR;
          flash_address_crc32 = flash_address;
          myState = DEVICE_UPDATE_STATE_ERASE_LOCK_FLASH;
        }
        else {
          flash_address += PAGE_SIZE;
          myState = DEVICE_UPDATE_STATE_ERASE_FLASH;
        }
      }
      else if ( (FLASH_ERROR_PROGRAM == FLASH_GetStatus()) ||
                (FLASH_ERROR_WRP == FLASH_GetStatus()) ) 
      {
        error = 1;
        Finish();
      }
      break;
    case DEVICE_UPDATE_STATE_ERASE_LOCK_FLASH:
      FLASH_Lock();
      flash_address = APP_MIRROR;
      Finish();
      break;
    
    /* write */
    case DEVICE_UPDATE_STATE_WRITE_INIT:
      if (((uint32_t)flash_address + (nr_of_uint32 * 4)) <= (APP_MIRROR + APP_SIZE)) {
        offset = 0;
        myState = DEVICE_UPDATE_STATE_WRITE;
      }
      else {
        error = 1;
        Finish();
      }
      break;
    case DEVICE_UPDATE_STATE_WRITE:
      if (offset < nr_of_uint32) {
        FLASH_Unlock();
        FLASH_ProgramWord((uint32_t)flash_address, p_data[offset]);
        offset += 1;
        flash_address += 4;
        FLASH_Lock();
        if ( (FLASH_ERROR_PROGRAM == FLASH_GetStatus()) ||
             (FLASH_ERROR_WRP == FLASH_GetStatus()) ) 
        {
          error = 1;
          Finish();
        }
      }
      else {
        CRC_ResetDR();
        crc32 = CRC_CalcBlockCRC((uint32_t*)flash_address_crc32, nr_of_uint32);
        if (crc32 == valid_crc32) {
          flash_address_crc32 = flash_address;
          myState = DEVICE_UPDATE_STATE_INIT;
        }
        else {
          error = 1;
        }
        Finish();
      }
      break;
    case DEVICE_UPDATE_STATE_CALC_CRC32:
      CRC_ResetDR();
      fw_crc32 = CRC_CalcBlockCRC((uint32_t*)APP_MIRROR, ((uint32_t)flash_address - APP_MIRROR)/4);
      Finish();
      break;
    case DEVICE_UPDATE_STATE_FLASH:
      update.Flash();
      break;
    default:
      break;
  }
}


/**
*/
uint32_t C_DEVICE_UPDATE::GetFirmwareCRC32()
{
  return fw_crc32;
}


/**
*/
bool C_DEVICE_UPDATE::IsReady(uint8_t *err)
{
  *err = error;
  return finished;
}


/* Private function ----------------------------------------------------------*/ 

/**
*/
void C_DEVICE_UPDATE::Finish()
{
  finished = true;
  myState = DEVICE_UPDATE_STATE_INIT;
}

/******************************************************************************* 
                             END_OF_FILE 
*******************************************************************************/ 
