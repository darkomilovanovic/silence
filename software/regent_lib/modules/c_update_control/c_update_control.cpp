#include <stdint.h> 
#include <stdio.h> 
#include <stdlib.h> 

/* Includes ------------------------------------------------------------------*/ 
#include "c_module/c_module.hpp"
#include "c_usart/c_usart.hpp"
#include "c_modbus_frame/c_modbus_frame.hpp"
#include "c_modbus/c_modbus.hpp"
#include "c_device_update\c_device_update.hpp"
#include "c_update_control\c_update_control.hpp"

/* Constructors etc. ---------------------------------------------------------*/ 
/** Constructor. 
  @return Class object. 
*/ 
C_UPDATE_CONTROL::C_UPDATE_CONTROL(C_DEVICE_UPDATE *pUpdate, C_MODBUS* pModbus) { 
  myDeviceUpdate = pUpdate;
  myModbus = pModbus;
  Reset();
}

/* Public function -----------------------------------------------------------*/ 

/** Reset 
*/
void C_UPDATE_CONTROL::Reset()
{
  myState = UP_STATE_INIT;
  myHRDataAddress = C_MODBUS::HREG_FW_VERSION;

  return;
}/*reset*/


/** Handler
*/
void C_UPDATE_CONTROL::Handler()
{
  uint8_t error = 0;
  
  switch (myState) {
    case UP_STATE_INIT:
      myState = UP_STATE_READY;
      myReceivedBytes = 0;
      myHRDataAddress = C_MODBUS::HREG_FW_UPDATE;
      myDeviceUpdate->Reset();
      myModbus->SetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE, FW_STATE_READY);
      break;
    case UP_STATE_READY:
      if (true == myModbus->GetHRValueModified(C_MODBUS::HREG_FW_UPDATE_STATE)) {
        if (FW_STATE_NEW_DATA == myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE)) {
          myState = UP_STATE_ERASE;
        }
        else if (FW_STATE_RESET == myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE)) {
          myModbus->EraseHRValueModifiedFlag(C_MODBUS::HREG_FW_UPDATE_STATE);
          myState = UP_STATE_INIT;
        }
      }
      break;
      
    /** ERASE MEMORY **/
    case UP_STATE_ERASE:
      if ( (true == myModbus->GetHRValueModified(C_MODBUS::HREG_FW_UPDATE_STATE)) &&
           (FW_STATE_RESET == myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE)) )
      {
        myModbus->EraseHRValueModifiedFlag(C_MODBUS::HREG_FW_UPDATE_STATE);
        myState = UP_STATE_INIT;
      }
      else {
        myDeviceUpdate->Erase();
        myState = UP_STATE_ERASE_WAIT;
      }
      break;
    case UP_STATE_ERASE_WAIT:
      if ( (true == myModbus->GetHRValueModified(C_MODBUS::HREG_FW_UPDATE_STATE)) &&
           (FW_STATE_RESET == myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE)) )
      {
        myModbus->EraseHRValueModifiedFlag(C_MODBUS::HREG_FW_UPDATE_STATE);
        myState = UP_STATE_INIT;
      }
      else if (true == myDeviceUpdate->IsReady(&error)) {
        if (0 != error) {
          myModbus->SetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE, FW_STATE_FAILURE);
          myState = UP_STATE_FAILURE;
        }
        else {
          myState = UP_STATE_RECEIVE;
        }
      }
      break;
      
    /** RECEIVE DATA **/
    case UP_STATE_RECEIVE:
      if (true == myModbus->GetHRValueModified(C_MODBUS::HREG_FW_UPDATE_STATE)) {
        myModbus->EraseHRValueModifiedFlag(C_MODBUS::HREG_FW_UPDATE_STATE);
        if (FW_STATE_RESET == myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE)) {
          myState = UP_STATE_INIT;
        }
        else if (FW_STATE_NEW_DATA == myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE)) {
          myReceivedBytes = Receive();
          if (0 == myReceivedBytes) {
            myModbus->SetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE, FW_STATE_FAILURE);
            myState = UP_STATE_FAILURE;
          }
          else {
            myState = UP_STATE_WRITE_DATA;
          }
        }
        else if (FW_STATE_PROGRAM == myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE)) {
          myState = UP_STATE_VERIFY_FIRMWARE;
        }
      }
      break;
      
    /** WRITE DATA **/
    case UP_STATE_WRITE_DATA:
      myDeviceUpdate->Write((uint32_t*)myData, myReceivedBytes/2, myReceivedCRC32);
      myState = UP_STATE_WRITE_DATA_WAIT;
      break;
    case UP_STATE_WRITE_DATA_WAIT:
      if (true == myModbus->GetHRValueModified(C_MODBUS::HREG_FW_UPDATE_STATE)) {
        myModbus->EraseHRValueModifiedFlag(C_MODBUS::HREG_FW_UPDATE_STATE);
        if (FW_STATE_RESET == myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE)) {
          myState = UP_STATE_INIT;
        }
      }
      else if (true == myDeviceUpdate->IsReady(&error)) {
        if (0 != error) {
          myModbus->SetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE, FW_STATE_FAILURE);
          myState = UP_STATE_FAILURE;
        }
        else {
          myState = UP_STATE_RECEIVE;
          myModbus->SetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE, FW_STATE_WAITING);
        }
      }
      break;

    /** VERIFY FIRMWARE **/
    case UP_STATE_VERIFY_FIRMWARE:
      myDeviceUpdate->CalcCRC32();
      myState = UP_STATE_VERIFY_FIRMWARE_WAIT;
      break;
    case UP_STATE_VERIFY_FIRMWARE_WAIT:
      if (true == myDeviceUpdate->IsReady(&error)) {
        if ( (0 != error) ||
             (true != FirmwareValid()) ) 
        {
          myModbus->SetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE, FW_STATE_FAILURE);
          myState = UP_STATE_FAILURE;
        }
        else {
          myState = UP_STATE_UPDATE;
        }
      }
      break;

    /** UPDATE **/
    case UP_STATE_UPDATE:
      myDeviceUpdate->Update();
      myState = UP_STATE_UPDATE_PROGRAMMING;
      break;
    
    /** UPDATE RUNNING **/
    case UP_STATE_UPDATE_PROGRAMMING:
      break;
    
    /** FAILURE **/
    case UP_STATE_FAILURE:
      if ( (true == myModbus->GetHRValueModified(C_MODBUS::HREG_FW_UPDATE_STATE)) &&
           (FW_STATE_RESET == myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE)) )
      {
        myState = UP_STATE_INIT;
      }
      break;
    default:
      break;
  }
  
  return;
}/*run*/

/* Private function ----------------------------------------------------------*/ 

/**
*/
uint8_t C_UPDATE_CONTROL::Receive() {
  uint8_t received = 0;
  uint16_t *p_data = (uint16_t*)myData;
  
  myHRDataAddress = C_MODBUS::HREG_FW_UPDATE;
  while ( ((C_MODBUS::HREG_FW_UPDATE + (UPDATE_FW_PART_LENGTH /2)) > myHRDataAddress) &&
    (true == myModbus->GetHRValueModified(myHRDataAddress)) )
  {
    myModbus->EraseHRValueModifiedFlag(myHRDataAddress);
    p_data[received] = myModbus->GetHRValue(myHRDataAddress);
    received++;
    myHRDataAddress++;
  }

  if (0 != received % 2) {
    // must be a multiple of uint32_t!
    received = 0;
    myModbus->SetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE, FW_STATE_FAILURE);
    myState = UP_STATE_FAILURE;
  }  
  else if ( (true == myModbus->GetHRValueModified(C_MODBUS::HREG_FW_UPDATE_CRC_LW)) &&
            (true == myModbus->GetHRValueModified(C_MODBUS::HREG_FW_UPDATE_CRC_HW)) )
  {
    myModbus->EraseHRValueModifiedFlag(C_MODBUS::HREG_FW_UPDATE_CRC_LW);
    myModbus->EraseHRValueModifiedFlag(C_MODBUS::HREG_FW_UPDATE_CRC_LW);
    myReceivedCRC32 = myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_CRC_LW);
    myReceivedCRC32 |= (myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_CRC_HW) << 16);
  }
  else {
    received = 0;
    myModbus->SetHRValue(C_MODBUS::HREG_FW_UPDATE_STATE, FW_STATE_FAILURE);
    myState = UP_STATE_FAILURE;
  }
  
  return received;
}


/**
*/
void C_UPDATE_CONTROL::TurnOffIRQs() {
  uint8_t irq;
  
  for (irq = (uint8_t)WWDG_IRQn; irq <= (uint8_t)CEC_IRQn; irq++) {
    NVIC_DisableIRQ ((IRQn_Type)irq);
  }
}

/**
*/
bool C_UPDATE_CONTROL::FirmwareValid()
{
  bool      result = false;
  uint32_t  crc32;
  
  if ( (true == myModbus->GetHRValueModified(C_MODBUS::HREG_FW_UPDATE_CRC_LW)) &&
       (true == myModbus->GetHRValueModified(C_MODBUS::HREG_FW_UPDATE_CRC_HW)) )
  {
    myModbus->EraseHRValueModifiedFlag(C_MODBUS::HREG_FW_UPDATE_CRC_LW);
    myModbus->EraseHRValueModifiedFlag(C_MODBUS::HREG_FW_UPDATE_CRC_LW);
    
    myReceivedCRC32 = myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_CRC_LW);
    myReceivedCRC32 |= (myModbus->GetHRValue(C_MODBUS::HREG_FW_UPDATE_CRC_HW) << 16);

    crc32 = myDeviceUpdate->GetFirmwareCRC32();

    if (crc32 == myReceivedCRC32) {
      result = true;
    }
  }
  
  return result;
}



/******************************************************************************* 
                             END_OF_FILE 
*******************************************************************************/ 
