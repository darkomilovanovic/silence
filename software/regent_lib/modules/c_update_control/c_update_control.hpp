/**
 * \class C_UPDATE_CONTROL
 *
 * \ingroup C_UPDATE_CONTROL
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_UPDATE_CONTROL implementation
 *
 * This class implements the controlling of the update mechanism
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef __C_UPDATE_CONTROL_H__ 
#define __C_UPDATE_CONTROL_H__ 

/** Class description.  
*/ 
class C_UPDATE_CONTROL { 
public:
  static const uint8_t UPDATE_FW_PART_LENGTH = 120;
public:
  typedef enum {
    FW_STATE_INIT                 = 0,
    FW_STATE_READY,
    FW_STATE_WAITING,
    FW_STATE_NEW_DATA,
    FW_STATE_PROGRAM,
    FW_STATE_FAILURE,
    FW_STATE_RESET
  } t_fw_state;

private:
  typedef enum {
    UP_STATE_INIT             = 0,
    UP_STATE_READY,
    UP_STATE_ERASE,
    UP_STATE_ERASE_WAIT,
    UP_STATE_RECEIVE,
    UP_STATE_RECEIVE_WAIT,
    UP_STATE_WRITE_DATA,
    UP_STATE_WRITE_DATA_WAIT,
    UP_STATE_WAIT,
    UP_STATE_VERIFY_FIRMWARE,
    UP_STATE_VERIFY_FIRMWARE_WAIT,
    UP_STATE_UPDATE,
    UP_STATE_UPDATE_PROGRAMMING,
    UP_STATE_FAILURE
  } t_update_state;

  C_DEVICE_UPDATE       *myDeviceUpdate;
#if !defined (WIN32)
  C_MODBUS              *myModbus;
#endif
  
  uint16_t              myHRDataAddress;
  uint32_t              myData[UPDATE_FW_PART_LENGTH /4];
  uint32_t              myReceivedCRC32;
  uint8_t               myReceivedBytes;

  t_update_state        myState;

public: 
  
  /// Constructor
  C_UPDATE_CONTROL();
#if !defined (WIN32)
  C_UPDATE_CONTROL(C_DEVICE_UPDATE *pUpdate, C_MODBUS* pModbus);
#endif

  void Handler();

private:
  
  void Reset();
  uint8_t Receive();
  void TurnOffIRQs();
  bool FirmwareValid();

  uint8_t get_max_bytes_per_modbus_access() { return UPDATE_FW_PART_LENGTH ; };
  uint8_t get_max_hr_for_update() { return UPDATE_FW_PART_LENGTH /2; };

};/*C_UPDATE_CONTROL*/ 

#endif /* __C_UPDATE_CONTROL_H__ */

/******************************************************************************* 
                             END_OF_FILE 
*******************************************************************************/ 
