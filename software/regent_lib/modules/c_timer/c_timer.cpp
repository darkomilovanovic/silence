/** \defgroup C_TIMER C_TIMER_PackageTitle
 *
 * \brief This is the implementation of the software timer 
 *        interface.
 */


#include "c_timer/c_timer.hpp"
#ifdef V0
  #include "v0/c_interface.hpp"
#endif

extern C_INTERFACE interface;

C_TIMER::C_TIMER(void)
{
  return;
}


C_TIMER::C_TIMER(uint16_t duration)
{
  myDuration = duration;
  myCountdown = 0;
  myState = STATE_OFF;
  return;
}


C_TIMER::~C_TIMER()
{
  myDuration = 0;
  myCountdown = 0;
  myState = STATE_OFF;
  return;
}

void C_TIMER::SetDuration(uint16_t duration)
{
  myDuration = duration;
}

C_TIMER::TIMER_STATE C_TIMER::GetState(void)
{
  return myState;
}

uint16_t C_TIMER::GetDurationTime(void)
{
  return myDuration;
}

uint16_t C_TIMER::GetRemainingTime(void)
{
  return myCountdown;
}

void C_TIMER::Start(void)
{
  myState = STATE_RUNNING;
  myCountdown = myDuration;
  // Send a referenz to the timer handler
  interface.timerTrigger.AddTimer(this);
  return;
}

void C_TIMER::Start(uint16_t duration)
{
  myDuration = duration;
  myCountdown = myDuration;
  myState = STATE_RUNNING;
  // Send a referenz to the timer handler
  interface.timerTrigger.AddTimer(this);
  return;
}

void C_TIMER::Restart(void)
{
  myState = STATE_RUNNING;
  myCountdown = myDuration;
  return;
}

void C_TIMER::Stop(void)
{
  myState = STATE_OFF;
  // Remove it from the timer handler
  interface.timerTrigger.RemoveTimer(this);
  return;
}

void C_TIMER::Trigger(uint16_t duration)
{
  if (myCountdown > duration) myCountdown -= duration;
  else if (0 < myCountdown) {
    myCountdown = 0;
    myState = STATE_OVER;
  }
}
