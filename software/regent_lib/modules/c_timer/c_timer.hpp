/**
 * \class C_TIMER
 *
 * \ingroup C_TIMER
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_TIMER implementation
 *
 * This class implements the software timer
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_TIMER_H
#define C_TIMER_H

#include <stdint.h>
#include <stddef.h>


class C_TIMER
{

public:

  enum TIMER_STATE {
    STATE_OFF,
    STATE_RUNNING,
    STATE_OVER
  };

private:
  uint16_t        myDuration;
  uint16_t        myCountdown;
  TIMER_STATE     myState;

public:

  /// Create an C_TIMER
  C_TIMER();
  C_TIMER(uint16_t duration);

  /// Delete an C_BUTTON
  ~C_TIMER();

  /// Get the state of the timer
  TIMER_STATE GetState(void);

  /// Get the duration time of the timer
  uint16_t GetDurationTime(void);

  /// Get remaining time of the timer
  uint16_t GetRemainingTime(void);

  /// Set the timer duration
  void SetDuration(uint16_t duration);

  /// Start the timer
  void Start(void);

  /// Start the timer with a give duration
  void Start(uint16_t duration);

  /// Restart the timer
  void Restart(void);
  
  /// Stop the timer
  void Stop(void);
  
  /// Trigger function
  void Trigger(uint16_t duration);

};  // end of class C_TIMER


#endif  // C_TIMER_H


