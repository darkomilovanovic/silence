/** \defgroup C_TIMER_TRIGGER C_TIMER_TRIGGER_PackageTitle
 *
 * \brief This is the implementation of the timer trigger class
 */
#include "c_timer_element/c_timer_element.hpp"

C_TIMER_ELEMENT::C_TIMER_ELEMENT()
{
  element = NULL;
  next = NULL;
}

C_TIMER_ELEMENT::~C_TIMER_ELEMENT()
{
  element = NULL;
  next = NULL;
}

