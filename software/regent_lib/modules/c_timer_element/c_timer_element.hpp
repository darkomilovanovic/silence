/**
 * \class C_TIMER_ELMENT
 *
 * \ingroup C_TIMER_ELMENT
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_TIMER_ELMENT implementation
 *
 * This class implements the element class to C_TIMERS
 * all timer instances.
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_TIMER_ELEMENT_H
#define C_TIMER_ELEMENT_H

#include <stdint.h>

#include "c_timer/c_timer.hpp"

class C_TIMER_ELEMENT
{
  public:
    C_TIMER *element;
    C_TIMER_ELEMENT *next;
  
  C_TIMER_ELEMENT();
  ~C_TIMER_ELEMENT();
};


#endif  // C_TIMER_ELEMENT_H

