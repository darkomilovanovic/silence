/**
 * \class C_LED
 *
 * \ingroup C_LED
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_LED implementation
 *
 * This class implements a button with a kind of states
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_LED_H
#define C_LED_H


#include "c_output/c_output.hpp"
#include "c_timer/c_timer.hpp"
#include "c_module/c_module.hpp"
#include "c_pwm/c_pwm.hpp"

enum LED_POLARITY {
  LED_POLARITY_NORMAL,
  LED_POLARITY_INVERTED
};

enum LED_MODE {
  LED_MODE_OFF,
  LED_MODE_GO_TO_OFF,
  LED_MODE_ON, 
  LED_MODE_SLOT
};

class C_LED : public C_MODULE
{

private:
  C_OUTPUT        *myOutput;
  C_PWM           *myPWMDevice;
  LED_POLARITY    myPolarity;
  C_TIMER         myTimer;
  uint8_t         mySlots;
  uint16_t        mySlotDuration;
  uint16_t        mySlotValues;
  uint8_t         mySlotPeriods;
  uint16_t        mySlotPosition;
  uint16_t 		  myIntervalls;
  LED_MODE        myMode;

public:

  /// Create an C_LED
  C_LED();
  C_LED(C_OUTPUT *output);
  C_LED(C_OUTPUT *output, LED_POLARITY polarity);
  C_LED(C_OUTPUT *output, LED_POLARITY polarity, C_PWM *myPWM);

/// Delete an C_LED
  ~C_LED();

  /// Set the polarity of the button
  void SetPolarity(LED_POLARITY polarity);

  /// Turn ON and OFF
  void Set(uint8_t state);

  /// Invert the LED
  void Invert();

  /// Configure the automatic slot LED timing
  void SetSlotTime();

  /// Reconfigure the LED blinking with the automatic slot handling
  uint8_t StartSlotHandling(uint16_t slotDuration, uint8_t slotNumber, uint16_t slotValues);

  /// Stop the LED blinking with the automatic slot handling
  void StopSlotHandling(void);
  
  /// Get the LED status
  LED_MODE GetLEDMode(void);
  /// Handler
  virtual void Handler(void);

};  // end of class C_LED

#endif  // C_LED_H

