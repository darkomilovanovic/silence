/** \defgroup C_LED C_LED_PackageTitle
 *
 * \brief This is the implementation of the button class
 */


#include "c_led/c_led.hpp"

C_LED::C_LED()
{
  myPolarity = LED_POLARITY_NORMAL;
  mySlots = 0;
  mySlotDuration = 0;
  mySlotValues = 0;
  mySlotPosition = 0;
  myIntervalls = 0;
  myMode = LED_MODE_OFF;
  
  return;
}

C_LED::C_LED(C_OUTPUT *output, LED_POLARITY polarity)
{
  myOutput = output;
  myPolarity = polarity;
  mySlots = 0;
  mySlotDuration = 0;
  mySlotValues = 0;
  mySlotPosition = 0;
  myIntervalls = 0;
  myMode = LED_MODE_OFF;
  if(myOutput != NULL)
    Set(polarity);
}

C_LED::C_LED(C_OUTPUT *output, LED_POLARITY polarity, C_PWM *myPWM)
{
  myOutput = output;
  myPolarity = polarity;
  mySlots = 0;
  mySlotDuration = 0;
  mySlotValues = 0;
  mySlotPosition = 0;
  myIntervalls = 0;
  myMode = LED_MODE_OFF;
  myPWMDevice = myPWM;
}

void C_LED::Set(uint8_t state) {

  if (LED_POLARITY_INVERTED == myPolarity) {
    state = !state;
  }
  if (state == 1)
    myMode = LED_MODE_ON;
  else
    myMode = LED_MODE_OFF;
  
  if(myOutput != NULL)
  {
    myOutput->Set(state);
    return;
  }    
  if (myPWMDevice != NULL && state != myPolarity)
    myPWMDevice->Start();   
  if (myPWMDevice != NULL && state == myPolarity)
    myPWMDevice->Stop();  
    
  return;
}

void C_LED::SetPolarity(LED_POLARITY polarity)
{
  myPolarity = polarity;
}

//void C_LED::Invert(void)
//{
//  uint8_t state;
//  
//  if(myOutput != NULL)
//    state = myOutput->Get();
//  state = !state;
//  if(myOutput != NULL)
//    myOutput->Set(state);

//  return;
//}

uint8_t C_LED::StartSlotHandling(uint16_t slotDuration, uint8_t NumberOfSlots, uint16_t slotValues) //200, 5 , 0x0001
{
  uint8_t result = 1;
  if(NumberOfSlots >= 16)
  {
	  return 0;
  }
  mySlotDuration = slotDuration;
  myTimer.SetDuration(mySlotDuration);

  mySlots = NumberOfSlots;
  mySlotValues = slotValues;
  mySlotPosition = 0;
  myMode = LED_MODE_SLOT;
  if (0 < myTimer.GetDurationTime()) {
    myTimer.Start();
    result = 0;
  }
  return result;
}

void C_LED::StopSlotHandling(void)
{
  myMode = LED_MODE_GO_TO_OFF;
  return;
}

LED_MODE C_LED::GetLEDMode(void)
{
  return myMode;
}
void C_LED::Handler(void)
{
  if (LED_MODE_SLOT == myMode)
  {
    if (C_TIMER::STATE_OVER == myTimer.GetState()) 
	{
      if (mySlotPosition >= mySlots) 
	    mySlotPosition = 0;
	  
      if (0 < (mySlotValues & (1 << mySlotPosition))) 
      {
          if(myOutput != NULL)
            myOutput->Set(1);
          if (myPWMDevice != NULL)
            myPWMDevice->Start();
        
      }
      else 
	    {
         if(myOutput != NULL)
           myOutput->Set(0);
         if (myPWMDevice != NULL)
           myPWMDevice->Stop();
      }
      mySlotPosition++;
      myTimer.Restart();
    }
  }
  else if (myMode == LED_MODE_GO_TO_OFF)
  {
	if (C_TIMER::STATE_OVER == myTimer.GetState()) 
	{
      if ((mySlotValues & (1 << mySlotPosition)) > 0) 
	    {
        if(myOutput != NULL)
          myOutput->Set(1);
        if (myPWMDevice != NULL)
          myPWMDevice->Start();
      }
      else 
	   {
       if(myOutput != NULL)
         myOutput->Set(0);
       if (myPWMDevice != NULL)
         myPWMDevice->Stop();
      }     
	  if (mySlotPosition == mySlots) 
	  {
		 myMode = LED_MODE_OFF;
		 myTimer.Stop();
	  }
	  else
	  {
		  myTimer.Restart();
	  }
	  mySlotPosition++;    
	}
  }
  else if (myMode == LED_MODE_OFF)
  {
	  
  }
  else //LED_MODE_ON
  {}
}
