/** \defgroup C_INTERFACE C_INTERFACE_PackageTitle
 *
 * \brief Interface class for the easytouch functionality
 */

#include <stdint.h>

#include "stm32f0xx_rcc.h"

#include "v0/c_interface.hpp"
#include "memory_definitions/memory_definitions.h"


C_INTERFACE::C_INTERFACE(void)
{
  myRuntime = 0;
  myRuntimeOverflow = 0;
  ClockInitialisation();
  PWMInitialisation();
  GPIOInitialisation();
  USARTInitialisation();
  NVICInitialisation();
  DMAComponentInitialisation(myDMABuffer);
  ADCComponentInitialisation();
  WatchdogInitialisation();

  Setup();

  return;
}


C_INTERFACE::~C_INTERFACE(void)
{
  return;
}


uint64_t C_INTERFACE::GetRuntime(void)
{
  return myRuntime;
}


void C_INTERFACE::RealocateVectorTable(void)
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  
  /// Initialise the vector table in the RAM
  for (int i=0; i<0xC0; i+=4) {
    *((int32_t*)(0x20000000 + i)) = *((int32_t*)(APP_RUNNING + i));
  }
  SYSCFG->CFGR1 |= SYSCFG_CFGR1_MEM_MODE;
  
}

void C_INTERFACE::ClockInitialisation(void)
{
  RCC_ClocksTypeDef RCC_Clocks;
  
  /* GPIO clocks */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
  
  /* ADC1 Periph clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
  
  /* DMA1 clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1 , ENABLE);
  
  /* Enable WWDG clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);
  
  /* Clock for the CRC32 */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);
  
  /* SysTick end of count event each 1ms */
  RCC_GetClocksFreq(&RCC_Clocks);
  SysTick_Config( RCC_Clocks.HCLK_Frequency / 1000 );
  
  /* TIM1 + TIM2 + TIM3 */
   RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 , ENABLE);
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2 , ENABLE);  
  return;
}

void C_INTERFACE::GPIOInitialisation(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  
  /* Inputs */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_12; //N_SENSE | VIN_PEAK | L_SENSE | VSYS_RDY
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4; //DALI RX 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  
  /* Outputs */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11; //SEG3_DIS | SEG2_DIS | SEG1_DIS??
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  GPIO_WriteBit(GPIOA, GPIO_Pin_9, Bit_RESET);
  GPIO_WriteBit(GPIOA, GPIO_Pin_10, Bit_RESET);
  GPIO_WriteBit(GPIOA, GPIO_Pin_11, Bit_RESET);
  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_5; //STAT_LED_G | STAT_LED_R | VOL_EN_L | DALI_TX
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  GPIO_WriteBit(GPIOB, GPIO_Pin_0, Bit_SET);
  GPIO_WriteBit(GPIOB, GPIO_Pin_1, Bit_SET);
  GPIO_WriteBit(GPIOB, GPIO_Pin_2, Bit_SET);
  GPIO_WriteBit(GPIOB, GPIO_Pin_5, Bit_RESET);

  
  /*ADC*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_3 | GPIO_Pin_4; //VIN_SENSE | VLED_SENSE | I_SENSE | HW_ID
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
      
      
  /* Functions for UART/ RX*/
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  // TX
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_2;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_Init(GPIOA, &GPIO_InitStructure); 
  
  /* PWM Pin PWM_VGDRV   TIM2_CH2    AF_2 pwm_vgdrv*/
     /* Outputs */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource3, GPIO_AF_2);
  
  /* PWM Pin PWM_DIMM_L   TIM1_CH1    AF_2 pwm_dimm2_L*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_2); 

   return;
}

void C_INTERFACE::ADCComponentInitialisation(void)
{
  ADC_InitTypeDef   ADC_InitStructure;
  
  /* ADCs DeInit */  
  ADC_DeInit(ADC1);
  
  /* Initialize ADC structure */
  ADC_StructInit(&ADC_InitStructure);
  
  /* Configure the ADC1 in continuous mode with a resolution equal to 12 bits  */
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStructure.ADC_ContinuousConvMode = ENABLE; 
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_ScanDirection = ADC_ScanDirection_Upward;
  //channel 0 to channel 18
  ADC_Init(ADC1, &ADC_InitStructure); 
  
  // ADC1 channels configuration, ADC-Kanal ID Reihenfolge entspricht bei ScanDirection_Foreward der Datenreihenfolge
	ADC_ChannelConfig( ADC1, ADC_Channel_0,	ADC_SampleTime_239_5Cycles );	// VIN_SENSE
  ADC_ChannelConfig( ADC1, ADC_Channel_1,	ADC_SampleTime_239_5Cycles );	// VLED_SENSE
	ADC_ChannelConfig( ADC1, ADC_Channel_3,	ADC_SampleTime_239_5Cycles );	// I_SENSE
  ADC_ChannelConfig( ADC1, ADC_Channel_4,	ADC_SampleTime_239_5Cycles );	// HW_ID
	ADC_ChannelConfig( ADC1, ADC_Channel_TempSensor,	ADC_SampleTime_239_5Cycles );	// Internal Temperatur	Array Index 0

   /* ADC Calibration */
  ADC_GetCalibrationFactor(ADC1);
  
  
  ADC_TempSensorCmd( ENABLE );
  
  /* ADC DMA request in circular mode */
  ADC_DMARequestModeConfig(ADC1, ADC_DMAMode_Circular);
  
    // Enable ADC_DMA
 	ADC_DMACmd( ADC1, ENABLE );
  
  
}

void C_INTERFACE::DMAComponentInitialisation(uint16_t myDMABuffer[])
{
  DMA_InitTypeDef   DMA_InitStructure;
  
  /* DMA1 Channel1 Config */
  DMA_DeInit(DMA1_Channel1);
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)ADC1_DR_Address;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)myDMABuffer;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
  DMA_InitStructure.DMA_BufferSize = NUMBER_OF_MEASURED_AD_CHANNELS;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(DMA1_Channel1, &DMA_InitStructure);
    
  /* DMA1 Channel1 enable */
  DMA_Cmd(DMA1_Channel1, ENABLE);
}
void C_INTERFACE::NVICInitialisation(void)
{
  NVIC_InitTypeDef  NVIC_InitStructure;
  
  NVIC_InitStructure.NVIC_IRQChannel = UINT8_MAX;

  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
  NVIC_Init(&NVIC_InitStructure);

  return;
}

void C_INTERFACE::USARTInitialisation(void)
{
  USART_InitTypeDef USART_InitStructure;
  USART_InitStructure.USART_BaudRate = 115200;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_2;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  // Initialise the USART interface
  USART_Init(USART2, &USART_InitStructure);

  // Initialise the interrupt
  USART_ITConfig(USART2, USART_IT_IDLE, ENABLE);
  USART_ITConfig(USART2, USART_IT_ORE, ENABLE);
  USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
  USART_ITConfig(USART2, USART_IT_PE, ENABLE);
  USART_ITConfig(USART2, USART_IT_LBD, ENABLE);
  USART_ITConfig(USART2, USART_IT_FE, ENABLE);
  USART_ITConfig(USART2, USART_IT_RTO, ENABLE);
  USART_ITConfig(USART2, USART_IT_TC, DISABLE);
  USART_ITConfig(USART2, USART_IT_TXE, DISABLE);

  return;
}

void C_INTERFACE::PWMInitialisation(void)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  TIM_OCInitTypeDef  TIM_OCInitStructure;
  uint16_t ChannelPulse = 0;
  uint32_t TimerPeriode = (SystemCoreClock / (PWM_FREQUENCY_10K + 1));
   /* Compute the value to be set in ARR regiter to generate signal frequency at 10 kHz */
  // -> PWM_FREQUENCY = (SystemCoreClock / 0x12C0 ) - 1; //
  ChannelPulse = 0;
    
  TIM_TimeBaseStructure.TIM_Prescaler = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = (uint16_t)TimerPeriode;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
  
  TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure); //-> Channel 1
 
  TimerPeriode = (SystemCoreClock / (PWM_FREQUENCY_100K + 1));
  TIM_TimeBaseStructure.TIM_Period = (uint16_t)TimerPeriode;
  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //-> Channel 2
  
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
  TIM_OCInitStructure.TIM_Pulse = ChannelPulse;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //TIM_OCPolarity_High
  TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
  TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set; //TIM_OCIdleState_Reset
  TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset;
  
  TIM_OC1Init(TIM1, &TIM_OCInitStructure); // PWM_DIMM_L
  
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low; //TIM_OCPolarity_High
  TIM_OC2Init(TIM2, &TIM_OCInitStructure); // PWM_VGDRV

  /* TIM1 counter enable */
  TIM_Cmd(TIM1, ENABLE);
  
  /* TIM2 counter enable */
  TIM_Cmd(TIM2, ENABLE);
}
void C_INTERFACE::WatchdogInitialisation(void)
{
   /* WWDG clock counter = (PCLK1 (48MHz)/4096)/8 = 1464Hz (~683 us)  */
  //->48MHz/4096/4 = 2929 Hz == 341us
  WWDG_SetPrescaler(WWDG_Prescaler_4);

  /* Set Window value to 80; WWDG counter should be refreshed only when the counter
    is below 80 (and greater than 64) otherwise a reset will be generated */
  WWDG_SetWindowValue(80);

  /* Enable WWDG and set counter value to 127, WWDG timeout = ~683 us * 64 = 43.7 ms 
     In this case the refresh window is: ~683 * (127-80)= 32.1ms < refresh window < ~683 * 64 = 43.7ms
     */
  //WWDG_Enable(127);
}
void C_INTERFACE::Setup(void)
{
  // Mapping physical interface to instances

  //input = new C_INPUT(GPIOA, GPIO_Pin_1);
  n_sense = new C_INPUT(GPIOA, GPIO_Pin_5);
  vin_peak = new C_INPUT(GPIOA, GPIO_Pin_6);
  l_sense = new C_INPUT(GPIOA, GPIO_Pin_7);
  vsys_rdy = new C_INPUT(GPIOA, GPIO_Pin_12);
  
  vol_en_L = new C_OUTPUT(GPIOB, GPIO_Pin_2);
  em_en = new C_OUTPUT(GPIOB, GPIO_Pin_8);
  seg3dis = new C_OUTPUT(GPIOA, GPIO_Pin_9);
  seg2dis = new C_OUTPUT(GPIOA, GPIO_Pin_10);
  seg1dis = new C_OUTPUT(GPIOA, GPIO_Pin_11);
  
  led_outputs[0] = new C_OUTPUT(GPIOB, GPIO_Pin_0);
  led_outputs[1] = new C_OUTPUT(GPIOB, GPIO_Pin_1);
  
  usart2 = new C_USART(USART2, NULL);
  usart2->SetMode(C_USART::MODE_USART);
 
  //button = new C_BUTTON(input, BUTTON_POLARITY_INVERTED, modbus->GetHRValue(C_MODBUS::C_MODBUS::HREG_TIME_FOR_SHORT_PRESS_MIN), modbus->GetHRValue(C_MODBUS::HREG_TIME_FOR_SHORT_PRESS_MAX),modbus->GetHRValue(C_MODBUS::HREG_TIME_FOR_LONG_PRESS_MIN), modbus->GetHRValue(C_MODBUS::HREG_TIME_FOR_LONG_PRESS_MAX));
   
  pwm_dimm_L = new C_PWM(TIM1, PWM_FREQUENCY_10K, 1, 0, 100); // Timer1_CH1, Power ON Level = 0%, Power Level running mode = 100%
  pwm_vgdrv = new C_PWM(TIM2, PWM_FREQUENCY_100K, 2, 0 , 100); //Timer2, CH2, Power ON Level = 0%, Power Level running mode = 100%
  
  stat_led_g_L = new C_LED(led_outputs[0], LED_POLARITY_INVERTED);
  stat_led_r_L = new C_LED(led_outputs[1], LED_POLARITY_INVERTED);

  vin_sense = new C_ADC(myDMABuffer, 0, VOLTAGE); //channel 0
  vled_sense = new C_ADC(myDMABuffer, 1, VOLTAGE); //channel 5
  i_sense = new C_ADC(myDMABuffer, 2, CURRENT); //channel 6
  hwVersion = new C_ADC(myDMABuffer, 3, VOLTAGE); //channel 7
  temperature = new C_ADC(myDMABuffer, 4, TEMPERATURE); //temp

  analogMeasurings = new C_ADC_COMPONENT(ADC1);
  
  hwSpecific = new C_HW_SPECIFIC();
}

void C_INTERFACE::Handler(void)
{
 
  //PWM:
  pwm_dimm_L -> Handler();
  pwm_vgdrv -> Handler();
  

  //INPUTS GPIO:
  n_sense->Handler();
  vin_peak->Handler();
  l_sense->Handler();
  vsys_rdy->Handler();
  
  //ADC:
  vin_sense->Handler();
  vled_sense->Handler(); //channel 5
  i_sense->Handler(); //channel 6 
  hwVersion->Handler(); //channel 7
  temperature->Handler(); //temp
  analogMeasurings->Handler();
    
 //OUTPUTS GPIO:
  led_outputs[0]->Handler();
  led_outputs[1]->Handler();
  stat_led_g_L->Handler();
  stat_led_r_L->Handler();
  
  vol_en_L ->Handler();
  em_en->Handler();
  seg3dis->Handler();
  seg2dis->Handler();
  seg1dis->Handler();
  
  hwSpecific->Handler();
}

void C_INTERFACE::Trigger(uint16_t milliseconds)
{
  if (myRuntime > UINT64_MAX - milliseconds) {
    myRuntimeOverflow = 1;
  }
  myRuntime += milliseconds;

  timerTrigger.Trigger(milliseconds);

  return;
}

extern "C" 
{
extern C_INTERFACE interface;
static uint8_t sysTick1MsCounter = 0;

  void SysTick_Handler(void)
  {
    sysTick1MsCounter++;
    if (0 == (sysTick1MsCounter % 10)) {
      // Do mandatory stuff
      interface.timerTrigger.Trigger(10);

      sysTick1MsCounter = 0;
    }
    interface.hwSpecific->Handler();
    return;
  }

  void USART2_IRQHandler(void)
  {
    interface.usart2->Interrupt();
    return;
  }
}
