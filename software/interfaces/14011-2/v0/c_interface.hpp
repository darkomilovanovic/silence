/**
 * \class C_INTERFACE
 *
 * \ingroup C_INTERFACE
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_INTERFACE implementation
 *
 * This class implements the interface to the hardware for the application
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_INTERFACE_H
#define C_INTERFACE_H

#include "stm32f0xx_gpio.h"

#include "c_timer_trigger/c_timer_trigger.hpp"
#include "c_input/c_input.hpp"
#include "c_output/c_output.hpp"
#include "c_module/c_module.hpp"
#include "c_usart/c_usart.hpp"
#include "c_button/c_button.hpp"
#include "c_led/c_led.hpp"
#include "c_adc_component/c_adc_component.hpp"
#include "c_hw_specific/c_hw_specific.hpp"
#include "c_pwm/c_pwm.hpp"


class C_INTERFACE
{

public:
  // mandatory
  uint64_t          myRuntime;          // ms
  uint8_t           myRuntimeOverflow;  // 
  C_TIMER_TRIGGER   timerTrigger;
  uint16_t          myDMABuffer[NUMBER_OF_MEASURED_AD_CHANNELS];

  // physical interface
//not used:
  C_INPUT           *input;
  C_INPUT           *dali_rx;
  C_OUTPUT          *dali_tx_L;

//used:
  C_INPUT           *n_sense;
  C_INPUT           *vin_peak;
  C_INPUT           *l_sense;
  C_INPUT           *vsys_rdy;

  C_OUTPUT          *led_outputs[2];
  C_OUTPUT          *vol_en_L;
  C_OUTPUT          *em_en;
  C_OUTPUT          *seg3dis;
  C_OUTPUT          *seg2dis;
  C_OUTPUT          *seg1dis;
  C_USART           *usart2;

  // framework interface
  //C_BUTTON          *buttonp;
  C_LED             *stat_led_g_L;
  C_LED             *stat_led_r_L;
  C_HW_SPECIFIC     *hwSpecific;
  C_ADC             *vin_sense;
  C_ADC             *vled_sense;
  C_ADC             *i_sense;
  C_ADC             *hwVersion;
  C_ADC             *temperature;
  C_ADC_COMPONENT   *analogMeasurings;
  C_PWM             *pwm_vgdrv;
  C_PWM             *pwm_dimm_L;

private:

  /**
    * General clock setup
    * @param[in] none
    * @return none
    */
  void ClockInitialisation(void);

  /// Realocate the vector table to the SRAM
  void RealocateVectorTable(void);

  /**
    * Setup of the gpio pins and ports
    * @param[in] none
    * @return none
    */
  void GPIOInitialisation(void);

  /**
    * General NVIC initialisation
    * @param[in] none
    * @return none
    */
  void NVICInitialisation(void);

  /**
    * General USART initialisation
    * @param[in] none
    * @return none
    */
  void ADCComponentInitialisation(void);
  /**
    * General ADC initialisation
    * @param[in] none
    * @return none
    */
  void DMAComponentInitialisation(uint16_t myDMABuffer[]);
  /**
    * General DMA initialisation
    * @param[in] none
    * @return none
    */
  void USARTInitialisation(void);
  
  /**
    * Setup of the interface
    */
  void Setup(void);
  
  /**
    * Periodically trigger
    */
  void Trigger(uint16_t milliseconds);
  
  /**
    * Get the total runtime of the application
    */
  void PWMInitialisation(void);
  void WatchdogInitialisation(void);
  uint64_t GetRuntime(void);

public:

  /// Constructor
  C_INTERFACE(void);

  /// Destructor
  ~C_INTERFACE(void);

  /// Global handler for all interfaces
  void Handler(void);

};  // end of class C_INTERFACE

#endif  // C_INTERFACE_H

