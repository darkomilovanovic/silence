
/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx.h"
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "memory_definitions/memory_definitions.h"
//#include "version_handler/version_handler.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static unsigned int systick = 0;
static unsigned int toggleFlag = 0;
/* Private function prototypes -----------------------------------------------*/
static void IAP_Init(void);
static void IAP_DeInit(void);

/* Private functions ---------------------------------------------------------*/

/* Global functions ----------------------------------------------------------*/
extern pFunction Jump_To_Application;
extern uint32_t JumpAddress;

void SysTick_Handler(void)
{
  systick++;
}

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
    int ledCounter = 10;

    /* Flash unlock */
    FLASH_Unlock();
    /* Execute the IAP driver in order to re-program the Flash */
    IAP_Init();
  
    /* Init the version handling */
    //version_handler_init();


    while (0 < ledCounter) {
      if (50 <= systick) {
        systick = 0;
        if(toggleFlag == 0)
          {
            LED_GREEN_OFF();
            LED_RED_ON();
            toggleFlag = 1;
          }
          else
          {
            LED_GREEN_ON();
            LED_RED_OFF();
            toggleFlag = 0;
          }
        ledCounter--;
      }
    }
  
    if (*((unsigned int *)0x20001FF0) == 0x12345678) // Bootloader on demand active 
    {
        *((unsigned int *)0x20001FF0) = 0;  // Erase Bootloader RAM pattern
        SerialPutString("Booting.....\n\r");
    }
    else 
    {
      LED_GREEN_OFF();
      LED_RED_OFF();
      // Test if Application code is existing
      if ( (((*(__IO uint32_t*)APP_RUNNING) & 0x2FFE0000 ) == 0x20000000))
      { 
        IAP_DeInit();
        /* Jump to user application */
        JumpAddress = *(__IO uint32_t*) (APP_RUNNING + 4);
        Jump_To_Application = (pFunction) JumpAddress;
        /* Initialize user application's Stack Pointer */
        LED_GREEN_OFF();
        LED_RED_OFF();
        __set_MSP(*(__IO uint32_t*) APP_RUNNING);

        Jump_To_Application();
      } // if no code exists proceed with bootloader
    }

    Main_Menu ();
}

/**
  * @brief  Initialize the IAP: Configure RCC, USART and GPIOs.
  * @param  None
  * @retval None
  */
void IAP_Init(void)
{
  USART_InitTypeDef USART_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_ClocksTypeDef RCC_Clocks;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  TIM_OCInitTypeDef  TIM_OCInitStructure;
  
  uint16_t ChannelPulse = 0;
  uint16_t TimerPeriode = 0;
  
  /* GPIO clocks */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
  
  /* USART1 clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

  /* SysTick end of count event each 1ms */
  RCC_GetClocksFreq(&RCC_Clocks);
  SysTick_Config( RCC_Clocks.HCLK_Frequency / 1000 );
  
  /* TIM1 + TIM2 + TIM3 */
   RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 , ENABLE);
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2 , ENABLE);
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 , ENABLE);
  
  /* USART1 for RS485 */
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  // Driver Enable Pin
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12; 
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  GPIO_WriteBit(GPIOA, GPIO_Pin_12, Bit_RESET);
    
  /* PWM Pin PA8, LED Green*/
  /* Outputs */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 ;//LED Green
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_2);
  
  /* PWM Pin PB3 + PB4 - LED Blue + LED Red*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_4 ;//LED Blue + LED Red
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource3, GPIO_AF_2); 
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource4, GPIO_AF_1);
  
   /* Compute the value to be set in ARR regiter to generate signal frequency at 10 kHz */
  // -> PWM_FREQUENCY = (SystemCoreClock / 0x12C0 ) - 1; //
  TimerPeriode = (SystemCoreClock / (PWM_FREQUENCY + 1));
  ChannelPulse = 0;
  
  TIM_TimeBaseStructure.TIM_Prescaler = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = TimerPeriode;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
  
  TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure); //-> Channel 1
  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //-> Channel 2
  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); //-> Channel 1
  
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
  TIM_OCInitStructure.TIM_Pulse = ChannelPulse;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low; //TIM_OCPolarity_High
  TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
  TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set; //TIM_OCIdleState_Reset
  TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset;
  
  TIM_OC1Init(TIM1, &TIM_OCInitStructure); // Channel 1 -> green
  TIM_OC2Init(TIM2, &TIM_OCInitStructure); // Channel 2 -> blue
  TIM_OC1Init(TIM3, &TIM_OCInitStructure); // Channel 1 -> red

  /* TIM1 counter enable */
  TIM_Cmd(TIM1, ENABLE);
  
  /* TIM2 counter enable */
  TIM_Cmd(TIM2, ENABLE);
  
  /* TIM3 counter enable */
  TIM_Cmd(TIM3, ENABLE);
  
  TIM_SetCompare1(TIM1, 0);
  TIM_SetCompare2(TIM2, 0);
  TIM_SetCompare1(TIM3, 0);
  
  TIM_CtrlPWMOutputs(TIM1, ENABLE);
  TIM_CtrlPWMOutputs(TIM2, ENABLE);
  TIM_CtrlPWMOutputs(TIM3, ENABLE);

  SNSREN();

  LED_GREEN_ON();
  LED_RED_OFF();

  /* USART resources configuration (Clock, GPIO pins and USART registers) ----*/
  /* USART configured as follow:
        - BaudRate = 115200 baud  
        - Word Length = 8 Bits
        - One Stop Bit
        - No parity
        - Hardware flow control disabled (RTS and CTS signals)
        - Receive and transmit enabled
  */
  USART_InitStructure.USART_BaudRate = 115200;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  USART_Init(USART1,&USART_InitStructure);

  /* Enable USART */
  USART_Cmd(USART1, ENABLE);

}

/**
  * @brief  Initialize the IAP: Configure RCC, USART and GPIOs.
  * @param  None
  * @retval None
  */
void IAP_DeInit(void)
{
  /* USART1 clock */
  USART_Cmd(USART1, DISABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, DISABLE);

  /* GPIO clocks */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, DISABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, DISABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, DISABLE);
  
}

#ifdef USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
