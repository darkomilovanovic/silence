/**
 * \class C_INPUT
 *
 * \ingroup C_INPUT
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_SILENCE implementation
 *
 * This class implements the application for running on the SILENCE hardware
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_SILENCE_H
#define C_SILENCE_H

#include "stm32f0xx_gpio.h"
#ifdef V0
  #include "v0/c_interface.hpp"
#endif
  
#include "c_version/c_version.hpp"


enum SILENCE_STATES {
  SILENCE_ERROR_STATE,
  SILENCE_UPDATE_STATE
};


class C_SILENCE
{

private:
  typedef enum {
    OP_MODE_NORMALE           = 0xFFFF,
    OP_MODE_FW_UPDATE         = 0x0100,
    OP_MODE_PASSIV_FW_UPDATE  = 0x0000,
    OP_MODE_FACTORY_RESET     = 0x0002,
    OP_MODE_RESET             = 0x0004
  } T_OP_MODE;


private:

  C_INTERFACE       *myInterface;

  C_VERSION         myVersion;


public:

  /// Constructor
  C_SILENCE(void);

  /// Destructor
  ~C_SILENCE(void);

  /// Run the SILENCE application
  void Run(void);

  /// Application error function
  void Error(SILENCE_STATES error);

  /// Function that disable the ymodem interface
  void UpdateOfOtherDevices(SILENCE_STATES update);

  /// Function that starts the firmware update
  void StartUpdate(void);
  
  void StartApplication(C_INTERFACE *interface);

private:

  /// Handle the buttons
  void HandleButtons(void);

  void HandleInputs(void);
  void HandleOutputs(void);
  void HandlePWM(void);
  void HandleLEDs(void);

/// Handle the ADC
  void HandleADC(void);

  /// Handle the operating modes of the application
  void HandleOperatingMode(void);

  /// Handle the version of the application
  void HandleVersionString(void);

  void Reset(void);

  void HandleHWSpecifics(void);

  C_TIMER    my20msTimer;
  C_TIMER    my100msTimer;
  C_TIMER    my50msSingleShotTimer;
  C_TIMER    my50msTimer;
};  // end of class C_SILENCE

#endif  // C_SILENCE_H

