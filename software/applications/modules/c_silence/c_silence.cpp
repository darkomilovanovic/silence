/** \defgroup C_SILENCE C_SILENCE_PackageTitle
 *
 * \brief Application class for the easytouch functionality
 */

#include <stdio.h>
#include <string.h>

#include "c_silence/c_silence.hpp"
#include "c_input/c_input.hpp"
#include "c_module/c_module.hpp"
#include "c_usart/c_usart.hpp"
#include "c_modbus_frame/c_modbus_frame.hpp"
#include "c_modbus/c_modbus.hpp"
#include "c_adc/c_adc.hpp"
  
uint8_t buttonFlag = 0;
uint16_t buttons = 0;

C_SILENCE::C_SILENCE(void)
{
  my20msTimer.SetDuration(20);
  my50msTimer.SetDuration(50);
  my100msTimer.SetDuration(100);
  my50msSingleShotTimer.SetDuration(50);

  return;
}


C_SILENCE::~C_SILENCE(void)
{
  return;
}

void C_SILENCE::StartApplication(C_INTERFACE *interface)
{
  myInterface = interface;
  myInterface->analogMeasurings->Start();
  myInterface->pwm_vgdrv->Enable(); //LED GREEN
  myInterface->pwm_dimm_L->Enable(); //LED BLUE
  #if NOT_IN_DEBUG_MODE
    myInterface->hwSpecific->Start();
  #endif
  }

void C_SILENCE::Run(void)
{
  my20msTimer.Start();
  my50msSingleShotTimer.Start();
  my50msTimer.Start();
  my100msTimer.Start();

  while(1) 
  {
    // Mandatory 
    myInterface->Handler();
 
    // Application
    if(my20msTimer.GetState() == C_TIMER::STATE_OVER)
    {
      //HandleHWSpecifics();
        HandleLEDs();
      my20msTimer.Restart();
    } 
    if(my50msTimer.GetState() == C_TIMER::STATE_OVER)
    {
      HandleInputs();
      HandleOutputs();
      HandlePWM();
      my50msTimer.Restart();
    }    
    if(my100msTimer.GetState() == C_TIMER::STATE_OVER)
    {
      HandleADC();
      //HandleOperatingMode();
      my100msTimer.Restart();
    }
    if (my50msSingleShotTimer.GetState() == C_TIMER::STATE_OVER)
    {
      my50msSingleShotTimer.Stop();
      HandleVersionString(); 
    }
  }
}

void C_SILENCE::UpdateOfOtherDevices(SILENCE_STATES update)
{
  while(1) {
    myInterface->Handler();
  }
}

void C_SILENCE::StartUpdate(void)
{
  myInterface->hwSpecific->DisableInterrupts();
  *((unsigned int *)0x20001FF0) = 0x12345678;  // Bootloader
  myInterface->hwSpecific->Reset();
}

void C_SILENCE::Error(SILENCE_STATES error)
{
  while(1) {
	  // Mandatory
    myInterface->Handler();}
}

void C_SILENCE::HandleButtons(void)
{
}

void C_SILENCE::HandleInputs(void)
{
  uint8_t valueN_Sense = 0;
  uint8_t valueVIN_Peak = 0;
  uint8_t valueL_Sense = 0;
  uint8_t valueVsys_Rdy = 0;
  
  valueN_Sense = myInterface->n_sense->Get();
  valueVIN_Peak = myInterface->vin_peak->Get();
  valueL_Sense = myInterface->l_sense->Get();
  valueVsys_Rdy = myInterface->vsys_rdy->Get();
}

void C_SILENCE::HandleOutputs(void)
{
  myInterface->vol_en_L->Set(1); //1 = invertierenden pint auf 0 (aktiv) setzten, 0 = invertierenden pin auf 1 (inaktiv) setzen
  myInterface->em_en->Set(1);    //1 = aktiv setzen, high                          0 = inaktiv setzen, low
  myInterface->seg1dis->Set(0);
  myInterface->seg2dis->Set(1);
  myInterface->seg3dis->Set(1);  
}

void C_SILENCE::HandlePWM(void)
{
  myInterface->pwm_dimm_L->SetNewValue(1); //set pwm device to 10% power ->1us pro Puls
  myInterface->pwm_vgdrv->SetNewValue(1);  //set pwm device to 1% power ->82ns pro Puls
}

void C_SILENCE::HandleLEDs(void)
{
  myInterface->stat_led_g_L->Set(0); //1 = led ausschalten, 0 = led einschalten
  myInterface->stat_led_r_L->Set(1); //1 = led ausschalten, 0 = led einschalten
}

void C_SILENCE::HandleADC(void)
{
  if(myInterface->analogMeasurings->GetStateADC() == ADC_RUNNING) 
  {
    uint16_t temperature = 0;
    uint16_t vInSense = 0;
    uint16_t vLedSense = 0;
    uint16_t iSense = 0;
    temperature = myInterface->temperature->GetValue();
    vInSense = myInterface->vin_sense->GetValue();
    vLedSense = myInterface->vled_sense->GetValue();
    iSense = myInterface->i_sense->GetValue();
  }
}

void C_SILENCE::HandleOperatingMode(void)
{

}

void C_SILENCE::HandleVersionString(void)
{
  char *swVersion;
  char *swVersionBL;
  char *hwVersion;
  char hwVersionDefault[] = "14011-2xx";
  char *hwDefaultVersion = hwVersionDefault;
  
  swVersion = myVersion.GetSWVersion();
  swVersionBL = myVersion.GetSWVersionBL();
  hwVersion = myVersion.GetHWVersion();
}

void C_SILENCE::Reset(void)
{
  myInterface->hwSpecific->Reset();
}

void C_SILENCE::HandleHWSpecifics()
{
}
