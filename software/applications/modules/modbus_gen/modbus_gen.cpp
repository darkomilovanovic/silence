// 
//============================================================================== 
//	Copyright (c) Regent Lighting Ltd.
//============================================================================== 
/** \file	modbus_gen.cpp
@brief	Code generator for MODBUS interfaces
*******************************************************************************/
#include <stdio.h> 
#include <stdlib.h> 
#include <time.h>
#include <windows.h> 
/* Includes ------------------------------------------------------------------*/
#include "modbus_gen\modbus_gen.h" 
#include "CmdLine\CmdLine.h"


unsigned long error = 0;

/*******************************************************************************************************/
/* Input and output files.
/*******************************************************************************************************/

FILE* file_holding_register = NULL;
FILE* file_input_register = NULL;
FILE* file_coils = NULL;
FILE* file_discrete_inputs = NULL;

char fn_holding_register_vid[] = "hr.vid";
char fn_input_register_vid[] = "ir.vid";
char fn_coil_vid[] = "co.vid";
char fn_discrete_input_vid[] = "di.vid";

FILE* file_holding_register_vid = NULL;
FILE* file_input_register_vid = NULL;
FILE* file_coils_vid = NULL;
FILE* file_discrete_inputs_vid = NULL;

char fn_holding_register_attr[] = "hr.attr";
char fn_input_register_attr[] = "ir.attr";
char fn_coil_attr[] = "co.attr";
char fn_discrete_input_attr[] = "di.attr";

FILE* file_holding_register_attr = NULL;
FILE* file_input_register_attr = NULL;
FILE* file_coil_attr = NULL;
FILE* file_discrete_input_attr = NULL;


bool open_holding_register_for_vid(StringType filename)
{
  if ((file_holding_register = fopen(filename.c_str(), "r")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", filename.c_str()); error++;
    return false;
  }
  if ((file_holding_register_vid = fopen(fn_holding_register_vid, "w")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", fn_holding_register_vid); error++;
    fclose(file_holding_register);
    return false;
  }
  return true;
}

bool open_input_register_for_vid(StringType filename)
{
  if ((file_input_register = fopen(filename.c_str(), "r")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", filename.c_str()); error++;
    return false;
  }
  if ((file_input_register_vid = fopen(fn_input_register_vid, "w")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", fn_input_register_vid); error++;
    fclose(file_input_register);
    return false;
  }
  return true;
}

bool open_coil_for_vid(StringType filename)
{
  if ((file_coils = fopen(filename.c_str(), "r")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", filename.c_str()); error++;
    return false;
  }
  if ((file_coils_vid = fopen(fn_coil_vid, "w")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", fn_coil_vid); error++;
    fclose(file_coils);
    return false;
  }
  return true;
}

bool open_discrete_input_for_vid(StringType filename)
{
  if ((file_discrete_inputs = fopen(filename.c_str(), "r")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", filename.c_str()); error++;
    return false;
  }
  if ((file_discrete_inputs_vid = fopen(fn_discrete_input_vid, "w")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", fn_discrete_input_vid); error++;
    fclose(file_discrete_inputs);
    return false;
  }
  return true;
}


bool open_holding_register_for_attr(StringType filename)
{
  if ((file_holding_register = fopen(filename.c_str(), "r")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", filename.c_str()); error++;
    return false;
  }
  if ((file_holding_register_attr = fopen(fn_holding_register_attr, "w")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", fn_holding_register_attr); error++;
    fclose(file_holding_register);
    return false;
  }
  return true;
}

bool open_input_register_for_attr(StringType filename)
{
  if ((file_input_register = fopen(filename.c_str(), "r")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", filename.c_str()); error++;
    return false;
  }
  if ((file_input_register_attr = fopen(fn_input_register_attr, "w")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", fn_input_register_attr); error++;
    fclose(file_input_register);
    return false;
  }
  return true;
}

bool open_coil_for_attr(StringType filename)
{
  if ((file_coils = fopen(filename.c_str(), "r")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", filename.c_str()); error++;
    return false;
  }
  if ((file_coil_attr = fopen(fn_coil_attr, "w")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", fn_coil_attr); error++;
    fclose(file_coils);
    return false;
  }
  return true;
}

bool open_discrete_input_for_attr(StringType filename)
{
  if ((file_discrete_inputs = fopen(filename.c_str(), "r")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", filename.c_str()); error++;
    return false;
  }
  if ((file_discrete_input_attr = fopen(fn_discrete_input_attr, "w")) == NULL) {
    printf("ERROR - Cannot open file [%s]\n", fn_discrete_input_attr); error++;
    fclose(file_discrete_inputs);
    return false;
  }
  return true;
}

void close_holding_register()
{
  if (file_holding_register != NULL) fclose(file_holding_register);
  if (file_holding_register_vid != NULL) fclose(file_holding_register_vid);
  if (file_holding_register_attr != NULL) fclose(file_holding_register_attr);
  return;
}

void close_input_register()
{
  if (file_input_register != NULL) fclose(file_input_register);
  if (file_input_register_vid != NULL) fclose(file_input_register_vid);
  if (file_input_register_attr != NULL) fclose(file_input_register_attr);
  return;
}

void close_coil()
{
  if (file_coils != NULL) fclose(file_coils);
  if (file_coils_vid != NULL) fclose(file_coils_vid);
  if (file_coil_attr != NULL) fclose(file_coil_attr);
  return;
}

void close_discrete_input()
{
  if (file_discrete_inputs != NULL) fclose(file_discrete_inputs);
  if (file_discrete_inputs_vid != NULL) fclose(file_discrete_inputs_vid);
  if (file_discrete_input_attr != NULL) fclose(file_discrete_input_attr);
  return;
}


/*******************************************************************************************************/
/* Auxiliary global variables.
/*******************************************************************************************************/

char  inbuffer[5000];
char  name[500];
char  hexAddress[10];
char  shortname[500];
char  unit[500];
char  type[500];
char  acctype[500];

unsigned short address;
unsigned short index;
unsigned short min;
unsigned short max;
unsigned short def;

unsigned short addresses[2000];
unsigned short minvals[2000];
unsigned short maxvals[2000];
unsigned short defvals[2000];
unsigned short signedvals[2000];

/*******************************************************************************************************/
/* Generate identifiers for MODBUS variables.
/*******************************************************************************************************/

#if 0
/** Helper routine writing enum values.
*/
int generate_vid()
{
  unsigned int cnt, line;

  line = 0;
  while (!feof(fp_in)) {
    fgets(inbuffer, 5000, fp_in);
    line++;
    cnt = sscanf(inbuffer, "%[* #]");
    if (cnt == 0) {
      cnt = sscanf(inbuffer, "%[^;] ; %[^;] ; %d ; %d ; %[^;] ; %[^;] ; %d ", name, shortname, &address, &index, unit, type, &def);
      printf("[%i] %s|%s|%d|%d|%s|%s|%d\n", cnt, name, shortname, address, index, unit, type, def);
      if (cnt != 7) {
        printf("SYNTAX ERROR in line %d\n", line); error++;
        return 1;
      }
      fprintf(fp_out, "  %-30s = %d,   //%s\n", shortname, index, name);
    }
  }/*while*/
  return 0;
}/*generate_vid*/
#endif


/** Generate identifiers for MODBUS input registers.
*/
void generate_holding_register_vid(StringType filename)
{
  unsigned int i, cnt, line;

  if (open_holding_register_for_vid(filename) == false) {
    return;
  }
  fprintf(file_holding_register_vid, "/** MODBUS HOLDING REGISTERS (auto-generated).\n*/\ntypedef enum {\n");
  line = 0;
  i = 0;
  while (!feof(file_holding_register)) {
    inbuffer[0] = 0;
    fgets(inbuffer, 5000, file_holding_register);
    line++;
    if ((0 < strlen(inbuffer)) && (inbuffer != strchr(inbuffer, '#'))) {
      i++;
      cnt = sscanf(inbuffer, "%[^;] ; %[^;] ; %d ; %[^;] ; %d ; %[^;] ; %[^;] ; %i ; %i ; %i",
        name, shortname, &address, hexAddress, &index, unit, type, &min, &max, &def);
      printf("#%u : [%i] %s|%s|%d|%d|%s|%s|%d|%d|%d\n", i, cnt, name, shortname, address, index, unit, type, min, max, def);
      if (cnt != 10) {
        printf("SYNTAX ERROR in line %d\n", line); error++;
        break;
      }
      fprintf(file_holding_register_vid, "  HREG_%-30s = %d,   //%s[%s],%s,min=%u,max=%u,def=%u\n", shortname, index, name, unit, type, min, max, def);
    }
  }/*while*/
  if (error == 0) {
    fprintf(file_holding_register_vid, "  HoldingRegistersLastItem = %d\n} t_holding_register_id;\n\n", index + 1);
  }
  else {
    fprintf(file_holding_register_vid, "There were generation errors\n");
  }
  
  fprintf(file_holding_register_vid, "\ntypedef enum {\n");
  line = 0;
  i = 0;
  fseek(file_holding_register, 0, SEEK_SET);
  while (!feof(file_holding_register)) {
    inbuffer[0] = 0;
    fgets(inbuffer, 5000, file_holding_register);
    line++;
    if ((0 < strlen(inbuffer)) && (inbuffer != strchr(inbuffer, '#'))) {
      i++;
      cnt = sscanf(inbuffer, "%[^;] ; %[^;] ; %d ; %[^;] ; %d ; %[^;] ; %[^;] ; %i ; %i ; %i",
        name, shortname, &address, hexAddress, &index, unit, type, &min, &max, &def);
      printf("#%u : [%i] %s|%s|%d|%d|%s|%s|%d|%d|%d\n", i, cnt, name, shortname, address, index, unit, type, min, max, def);
      if (cnt != 10) {
        printf("SYNTAX ERROR in line %d\n", line); error++;
        break;
      }
      fprintf(file_holding_register_vid, "  HREG_ADDR_%-30s = 0x%04X,   //%s[%s],%s,min=%u,max=%u,def=%u\n", shortname, address, name, unit, type, min, max, def);
    }
  }/*while*/
  if (error == 0) {
    fprintf(file_holding_register_vid, "  HoldingRegistersAddressLastItem = %d\n} t_holding_register_address_id;\n\n", index + 1);
  }
  else {
    fprintf(file_holding_register_vid, "There were generation errors\n");
  }
  close_holding_register();

  return;
}/*generate_holding_register_vid*/


/** Generate identifiers for MODBUS input regisers.
*/
void generate_input_register_vid(StringType filename)
{
  unsigned int i, cnt, line;

  if (open_input_register_for_vid(filename) == false) {
    return;
  }
  fprintf(file_input_register_vid, "/** MODBUS INPUT REGISTERS (auto-generated).\n*/\ntypedef enum {\n");
  line = 0;
  i = 0;
  while (!feof(file_input_register)) {
    inbuffer[0] = 0;
    fgets(inbuffer, 5000, file_input_register);
    line++;
    cnt = sscanf(inbuffer, "%[* #]");
    if (cnt == 0) {
      i++;
      cnt = sscanf(inbuffer, "%[^;] ; %[^;] ; %d ;%[^;] ; %d ; %[^;] ; %[^;] ; %d ", name, shortname, &address, hexAddress, &index, unit, type, &def);
      printf("#%u : [%i] %s|%s|%d|%d|%s|%s|%d\n", i, cnt, name, shortname, address, index, unit, type, def);
      if (cnt != 8) {
        printf("SYNTAX ERROR in line %d\n", line); error++;
        break;
      }
      fprintf(file_input_register_vid, "  IREG_%-30s = %d,   //%s[%s], %s,def=%u\n", shortname, index, name, unit, type, def);
    }
  }/*while*/
  if (error == 0) {
    fprintf(file_input_register_vid, "  InputRegistersLastItem = %d\n} t_input_register_id;\n\n", index + 1);
  }
  else {
    fprintf(file_input_register_vid, "There were generation errors\n");
  }
  close_input_register();
  return;
}/*generate_input_register_vid*/


/** Generate identifiers for MODBUS coils.
*/
void generate_coil_vid(StringType filename)
{
  unsigned int i, cnt, line;

  if (open_coil_for_vid(filename) == false) {
    return;
  }
  fprintf(file_coils_vid, "/** MODBUS COILS (auto-generated).\n*/\ntypedef enum {\n");
  line = 0;
  i = 0;
  while (!feof(file_coils)) {
    inbuffer[0] = 0;
    fgets(inbuffer, 5000, file_coils);
    line++;
    cnt = sscanf(inbuffer, "%[* #]");
    if (cnt == 0) {
      i++;
      cnt = sscanf(inbuffer, "%[^;] ; %[^;] ; %d ; %[^;] ; %d ; %d ", name, shortname, &address, hexAddress, &index, &def);
      printf("#%u : [%i] %s|%s|%d|%d|%d\n", i, cnt, name, shortname, address, index, def);
      if (cnt != 6) {
        printf("SYNTAX ERROR in line %d\n", line); error++;
        break;
      }
      fprintf(file_coils_vid, "  COIL_%-30s = %d,   //%s, def=%u\n", shortname, index, name, def);
    }
  }/*while*/
  if (error == 0) {
    fprintf(file_coils_vid, "  CoilsLastItem = %d\n} t_coil_id;\n\n", index + 1);
  }
  else {
    fprintf(file_coils_vid, "There were generation errors\n");
  }
  close_coil();
  return;
}/*generate_coil_vid*/


/** Generate identifiers for MODBUS discrete inputs.
*/
void generate_discrete_input_vid(StringType filename)
{
  unsigned int i, cnt, line;

  if (open_discrete_input_for_vid(filename) == false) {
    return;
  }
  fprintf(file_discrete_inputs_vid, "/** MODBUS DISCRETE INPUTS (auto-generated).\n*/\ntypedef enum {\n");
  line = 0;
  i = 0;
  while (!feof(file_discrete_inputs)) {
    inbuffer[0] = 0;
    fgets(inbuffer, 5000, file_discrete_inputs);
    line++;
    cnt = sscanf(inbuffer, "%[* #]");
    if (cnt == 0) {
      i++;
      cnt = sscanf(inbuffer, "%[^;] ; %[^;] ; %d ; %[^;] ; %d ; %d", name, shortname, &address, hexAddress, &index, &def);
      printf("#%u : [%i] %s|%s|%d|%d|%d\n", i, cnt, name, shortname, address, index, def);
      if (cnt != 6) {
        printf("SYNTAX ERROR in line %d\n", line); error++;
        break;
      }
      fprintf(file_discrete_inputs_vid, "  DIN_%-30s = %d,   //%s\n", shortname, index, name);
    }
  }/*while*/
  if (error == 0) {
    fprintf(file_discrete_inputs_vid, "  DiscreteInputsLastItem = %d\n} t_discrete_input_id;\n\n", index + 1);
  }
  else {
    fprintf(file_discrete_inputs_vid, "There were generation errors\n");
  }
  close_discrete_input();
  return;
}/*generate_discrete_input_vid*/


/*******************************************************************************************************/
/* Generate attributes for MODBUS variables.
/*******************************************************************************************************/

/** Generate attributes for MODBUS holding regisers.
*/
void generate_holding_register_attr(StringType filename)
{
  unsigned int i, cnt, line, varcnt;

  if (open_holding_register_for_attr(filename) == false) {
    return;
  }
  line = 0;
  varcnt = 0;
  i = 0;
  while (!feof(file_holding_register)) {
    inbuffer[0] = 0;
    fgets(inbuffer, 5000, file_holding_register);
    line++;
    if ((0 < strlen(inbuffer)) && (inbuffer != strchr(inbuffer, '#'))) {
      i++;
      cnt = sscanf(inbuffer, "%[^;] ; %[^;] ; %d ; %[^;] ; %d ; %[^;] ; %[^;] ; %d ; %d ; %d", name, shortname, &address, hexAddress, &index, unit, type, &min, &max, &def);
      printf("#%u : [%i] %s|%s|%d|%d|%s|%s|%d\n", i, cnt, name, shortname, address, index, unit, type, def);
      if (cnt != 10) {
        printf("SYNTAX ERROR in line %u\n", line); error++;
        break;
      }
      addresses[varcnt] = address;
      defvals[varcnt] = def;
      minvals[varcnt] = min;
      maxvals[varcnt] = max;
      if (!strcmp(type, "signed")) {
        signedvals[varcnt] = 1;
      }
      else if (!strcmp(type, "unsigned")) {
        signedvals[varcnt] = 0;
      }
      else {
        error++;
        printf("Unexpected type value <%s>, must be signed or unsigned\n", type);
      }
      varcnt++;
    }
  }/*while*/

  if (error == 0) {
    fprintf(file_holding_register_attr, "/** Attributes for MODBUS HOLDING REGISTERS (auto-generated).\n*/\n");
    fprintf(file_holding_register_attr, "uint16_t s_modbusHRAddresses[] = {\n");
    fprintf(file_holding_register_attr, "  // holding register addresses");
    for (i = 0; i<varcnt; i++) {
      if (0 == (i % 8)) {
        fprintf(file_holding_register_attr, "\n  ");
      }
      fprintf(file_holding_register_attr, "0x%04X", addresses[i]);
      if (i+1 < varcnt) {
        fprintf(file_holding_register_attr, ", ");
      }
    }
    fprintf(file_holding_register_attr, "\n};\n\n");
    fprintf(file_holding_register_attr, "uint16_t s_modbusHRMinimumValues[] = {\n");
    fprintf(file_holding_register_attr, "  // holding register minimum values");
    for (i = 0; i<varcnt; i++) {
      if (0 == (i % 8)) {
        fprintf(file_holding_register_attr, "\n  ");
      }
      fprintf(file_holding_register_attr, "%5u", minvals[i]);
      if (i + 1 < varcnt) {
        fprintf(file_holding_register_attr, ", ");
      }
    }
    fprintf(file_holding_register_attr, "\n};\n\n");
    fprintf(file_holding_register_attr, "uint16_t s_modbusHRMaximumValues[] = {\n");
    fprintf(file_holding_register_attr, "  // holding register maximum values");
    for (i = 0; i<varcnt; i++) {
      if (0 == (i % 8)) {
        fprintf(file_holding_register_attr, "\n  ");
      }
      fprintf(file_holding_register_attr, "%5u", maxvals[i]);
      if (i + 1 < varcnt) {
        fprintf(file_holding_register_attr, ", ");
      }
    }
    fprintf(file_holding_register_attr, "\n};\n\n");
    fprintf(file_holding_register_attr, "uint16_t s_modbusHRDefaultValues[] = {\n");
    fprintf(file_holding_register_attr, "  // holding register default values");
    for (i = 0; i<varcnt; i++) {
      if (0 == (i % 8)) {
        fprintf(file_holding_register_attr, "\n  ");
      }
      fprintf(file_holding_register_attr, "%5d", defvals[i]);
      if (i + 1 < varcnt) {
        fprintf(file_holding_register_attr, ", ");
      }
    }
    fprintf(file_holding_register_attr, "\n};\n\n");
    fprintf(file_holding_register_attr, "uint16_t s_modbusHRValues[] = {\n");
    fprintf(file_holding_register_attr, "  // holding register values");
    for (i = 0; i<varcnt; i++) {
      if (0 == (i % 8)) {
        fprintf(file_holding_register_attr, "\n  ");
      }
      fprintf(file_holding_register_attr, "    0");
      if (i + 1 < varcnt) {
        fprintf(file_holding_register_attr, ", ");
      }
    }
    fprintf(file_holding_register_attr, "\n};\n\n");
    fprintf(file_holding_register_attr, "uint16_t s_modbusHRSignedFlags[] = {\n");
    fprintf(file_holding_register_attr, "  // signed flags");
    for (i = 0; i<varcnt; i++) {
      if (0 == (i % 8)) {
        fprintf(file_holding_register_attr, "\n  ");
      }
      fprintf(file_holding_register_attr, "%5d", signedvals[i]);
      if (i + 1 < varcnt) {
        fprintf(file_holding_register_attr, ", ");
      }
    }
    fprintf(file_holding_register_attr, "\n};\n\n");
  }
  else {
    fprintf(file_holding_register_attr, "GENERATION ERROR\n");
  }
  close_holding_register();
  return;
}/*generate_holding_register_attr*/


/** Generate attributes for MODBUS input registers.
*/
void generate_input_register_attr(StringType filename)
{
  unsigned int i, cnt, line, varcnt;

  if (open_input_register_for_attr(filename) == false) {
    return;
  }
  line = 0;
  varcnt = 0;
  i = 0;
  while (!feof(file_input_register)) {
    inbuffer[0] = 0;
    fgets(inbuffer, 5000, file_input_register);
    line++;
    cnt = sscanf(inbuffer, "%[* #]");
    if (cnt == 0) {
      i++;
      cnt = sscanf(inbuffer, "%[^;] ; %[^;] ; %d ; %[^;] ; %d ; %[^;] ; %[^;] ; %d ", name, shortname, &address, hexAddress, &index, unit, type, &def);
      printf("#%u : [%i] %s|%s|%d|%d|%s|%s|%d\n", i, cnt, name, shortname, address, index, unit, type, def);
      if (cnt != 8) {
        printf("SYNTAX ERROR in line %d\n", line); error++;
        break;
      }
      addresses[varcnt] = address;
      defvals[varcnt] = def;
      varcnt++;
    }
  }/*while*/

  if (error == 0) {
    fprintf(file_input_register_attr, "/** Attributes for MODBUS INPUT REGISTERS (auto-generated).\n*/\n");
    fprintf(file_input_register_attr, "const c_word_vars_def<c_bamo_modbus_vars::InputRegistersLastItem> c_bamo_modbus_vars::input_registers_def = {\n");
    fprintf(file_input_register_attr, "  //input register addresses\n  ");
    for (i = 0; i<varcnt; i++) {
      fprintf(file_input_register_attr, "%u,", addresses[i]);
    }
    fprintf(file_input_register_attr, "\n\n  //input register default values\n  ");
    for (i = 0; i<varcnt; i++) {
      if (i == varcnt - 1) {
        fprintf(file_input_register_attr, "%d", defvals[i]);
      }
      else {
        fprintf(file_input_register_attr, "%d,", defvals[i]);
      }
    }
    fprintf(file_input_register_attr, "\n};\n\n");
  }
  else {
    fprintf(file_input_register_attr, "GENERATION ERROR\n");
  }
  close_input_register();
  return;
}/*generate_input_register_attr*/


/** Generate attributes for MODBUS coils.
*/
void generate_coil_attr(StringType filename)
{
  unsigned int i, cnt, line, varcnt;

  if (open_coil_for_attr(filename) == false) {
    return;
  }
  line = 0;
  varcnt = 0;
  i = 0;
  while (!feof(file_coils)) {
    inbuffer[0] = 0;
    fgets(inbuffer, 5000, file_coils);
    line++;
    cnt = sscanf(inbuffer, "%[* #]");
    if (cnt == 0) {
      i++;
      cnt = sscanf(inbuffer, "%[^;] ; %[^;] ; %d ; %[^;] ; %d ; %d", name, shortname, &address, hexAddress, &index, &def);
      printf("#%u : [%i] %s|%s|%d|%d|%d\n", i, cnt, name, shortname, address, index, def);
      if (cnt != 6) {
        printf("SYNTAX ERROR in line %d\n", line); error++;
        break;
      }
      addresses[varcnt] = address;
      defvals[varcnt] = def;
      varcnt++;
    }
  }/*while*/

  if (error == 0) {
    fprintf(file_coil_attr, "/** Attributes for MODBUS COILS (auto-generated).\n*/\n");
    fprintf(file_coil_attr, "const c_bit_vars_def<c_bamo_modbus_vars::CoilsLastItem> c_bamo_modbus_vars::coils_def = {\n");
    fprintf(file_coil_attr, "  //coil addresses\n  ");
    for (i = 0; i<varcnt; i++) {
      fprintf(file_coil_attr, "%u,", addresses[i]);
    }
    fprintf(file_coil_attr, "\n\n  //coil default values\n  ");
    for (i = 0; i<varcnt; i++) {
      if (i == varcnt - 1) {
        fprintf(file_coil_attr, "%d", defvals[i]);
      }
      else {
        fprintf(file_coil_attr, "%d,", defvals[i]);
      }
    }
    fprintf(file_coil_attr, "\n};\n\n");
  }
  else {
    fprintf(file_coil_attr, "GENERATION ERROR\n");
  }
  close_coil();
  return;
}/*generate_coil_attr*/


/** Generate attributes for MODBUS discrete inputs.
*/
void generate_discrete_input_attr(StringType filename)
{
  unsigned int i, cnt, line, varcnt;

  if (open_discrete_input_for_attr(filename) == false) {
    return;
  }
  line = 0;
  varcnt = 0;
  i = 0;
  while (!feof(file_discrete_inputs)) {
    inbuffer[0] = 0;
    fgets(inbuffer, 5000, file_discrete_inputs);
    line++;
    cnt = sscanf(inbuffer, "%[* #]");
    if (cnt == 0) {
      i++;
      cnt = sscanf(inbuffer, "%[^;] ; %[^;] ; %d ; %[^;] ; %d ; %d ", name, shortname, &address, hexAddress, &index, &def);
      printf("#%u : [%i] %s|%s|%d|%d|%d\n", i, cnt, name, shortname, address, index, def);
      if (cnt != 6) {
        printf("SYNTAX ERROR in line %d\n", line); error++;
        break;
      }
      addresses[varcnt] = address;
      defvals[varcnt] = def;
      varcnt++;
    }
  }/*while*/

  if (error == 0) {
    fprintf(file_discrete_input_attr, "/** Attributes for MODBUS DISCRETE INPUTS (auto-generated).\n*/\n");
    fprintf(file_discrete_input_attr, "const c_bit_vars_def<c_bamo_modbus_vars::DiscreteInputsLastItem> c_bamo_modbus_vars::discrete_inputs_def = {\n");
    fprintf(file_discrete_input_attr, "  //discrete input addresses\n  ");
    for (i = 0; i<varcnt; i++) {
      fprintf(file_discrete_input_attr, "%u,", addresses[i]);
    }
    fprintf(file_discrete_input_attr, "\n\n  //discrete input default values\n  ");
    for (i = 0; i<varcnt; i++) {
      if (i == varcnt - 1) {
        fprintf(file_discrete_input_attr, "%d", defvals[i]);
      }
      else {
        fprintf(file_discrete_input_attr, "%d,", defvals[i]);
      }
    }
    fprintf(file_discrete_input_attr, "\n};\n\n");
  }
  else {
    fprintf(file_discrete_input_attr, "GENERATION ERROR\n");
  }
  close_discrete_input();
  return;
}/*generate_discrete_input_attr*/


void show_help(void)
{
  printf("USAGE:\n");
  printf("modbus_gen.exe [-hr holding_registers.csv] [-ir input_register.csv] [-co coil.csv] [-di discrete_input.csv]\n");
}

/** main
*/
int main(int argc, char **argv)
{
  CCmdLine cmdLine;
  StringType hr, ir, co, di;

  // parse the command line
  // use __argc and __argv, in MFC apps
  if (cmdLine.SplitLine(argc, argv) < 1)
  {
    // no switches were given on the command line, abort
    exit(-1);
  }

  // test for the 'help' case
  if (cmdLine.HasSwitch("-h"))
  {
    show_help();
    exit(0);
  }

  hr = cmdLine.GetSafeArgument("-hr", 0, "");
  ir = cmdLine.GetSafeArgument("-ir", 0, "");
  co = cmdLine.GetSafeArgument("-co", 0, "");
  di = cmdLine.GetSafeArgument("-di", 0, "");

  system("cd");

  if (0 != hr.compare("")) {
    printf("************************************************************\nHolding Register VIDs\n************************************************************\n");
    generate_holding_register_vid(hr);
    printf("************************************************************\nHolding Register ATTRs\n************************************************************\n");
    generate_holding_register_attr(hr);
  }
  if (0 != ir.compare("")) {
    printf("************************************************************\nInput Register VIDs\n************************************************************\n");
    generate_input_register_vid(ir);
    printf("************************************************************\nInput Register ATTRs\n************************************************************\n");
    generate_input_register_attr(ir);
  }
  if (0 != co.compare("")) {
    printf("************************************************************\nCoil VIDs\n************************************************************\n");
    generate_coil_vid(co);
    printf("************************************************************\nCoil ATTRs\n************************************************************\n");
    generate_coil_attr(co);
  }
  if (0 != di.compare("")) {
    printf("************************************************************\nDiscrete Input VIDs\n************************************************************\n");
    generate_discrete_input_vid(di);
    printf("************************************************************\nDiscrete Input ATTRs\n************************************************************\n");
    generate_discrete_input_attr(di);
  }


  printf("************************************************************\nConcatenate Files\n************************************************************\n");
  system("del MB_ATTR MB_VID MODBUS.attr MODBUS.vid >NUL 2>&1");
  system("copy /Y /B *.attr MB_ATTR");
  system("del *.attr >NUL 2>&1");
  system("ren MB_ATTR MODBUS.attr");
  system("copy /Y /B *.VID  MB_VID");
  system("del *.VID >NUL 2>&1");
  system("ren MB_VID MODBUS.vid");

  if (error == 0) {
    printf("************************************************************\nOK\n************************************************************\n");
  }
  else {
    printf("************************************************************\nERROR\n************************************************************\n");
  }

  fgetc(stdin);
  return error;
}/*main*/




/*******************************************************************************
END_OF_FILE
*******************************************************************************/
