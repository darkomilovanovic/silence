/**
 *
 * \brief Defines the memory (flash & sram) parameters for the device
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef MEMORY_DEFINITIONS_H
#define MEMORY_DEFINITIONS_H

#define APP_MIRROR          0x08008000
#define APP_RUNNING         0x08002000
#define BOOTLOADER          0x08000000
#define APP_SIZE            0x6000
#define PAGE_SIZE           0x400

#define VERSION_OFFSET      0x200
#define VERSION_ADDRESS     APP_RUNNING + VERSION_OFFSET
#define VERSION_ADDRESS_BL  BOOTLOADER + VERSION_OFFSET

#define SRAM_RUNNING        0x20000000

#define IMAGE_HEADER_SIZE   128

#endif  // MEMORY_DEFINITIONS_H

