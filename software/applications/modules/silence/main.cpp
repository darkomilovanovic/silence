/**
 * \brief silence_app Application code for the easy touch
 *
 * This is the implementation of the silence application
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#include "c_silence/c_silence.hpp"
#ifdef V0
  #include "v0/c_interface.hpp"
#endif

C_INTERFACE interface;
C_SILENCE silence;

/** This is the main application for the easytouch
  */
int main(void) {
  
  int result = 0;
  silence.StartApplication(&interface);
  silence.Run();  
  return result;
}
