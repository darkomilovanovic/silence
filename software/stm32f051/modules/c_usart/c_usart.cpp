/** \defgroup C_USART C_USART_PackageTitle
 *
 * \brief Provide some stuff to do stuff
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "c_usart/c_usart.hpp"



C_USART::C_USART(USART_TypeDef* usartx, C_OUTPUT *driverEnable)
{
  // USART definitions
  myUsart = usartx;
  myDriverEnable = driverEnable;
  
  myMode = MODE_USART;

  // Counters
  myIdleLineDetectionError = 0;
  myOverrunError = 0;
  myParityError = 0;
  myLinBreakError = 0;
  myNoiseFlagError = 0;
  myFramingError = 0;
  myReceiverTimeout = 0;
  myRxCounter = 0;
  myTxCounter = 0;
  myTcCounter = 0;
  
  // ModBus RTU mode
  myFrameAvailable = 0;
  USART_SetReceiverTimeOut(myUsart, 87);

  return;
}

void C_USART::SetMode(C_USART::MODE mode)
{
  myMode = mode;
  switch (myMode) {
    case MODE_USART:
      USART_ReceiverTimeOutCmd(myUsart, DISABLE);
      break;
    case MODE_MODBUS_RTU:
      USART_ReceiverTimeOutCmd(myUsart, ENABLE);
      break;
    default:
      break;
  }
}

void C_USART::Enable(void)
{
  if (MODE_MODBUS_RTU == myMode) {
    if (NULL != myDriverEnable) {
      myDriverEnable->Set(0);   // Receive
    }
    else {
      USART_DECmd(myUsart, ENABLE);
      USART_DEPolarityConfig(myUsart, USART_DEPolarity_High);
      USART_SetDEAssertionTime(myUsart, 0x1F);
      USART_SetDEDeassertionTime(myUsart, 0x1F);
    }
  }
  USART_Cmd(myUsart, ENABLE);

}

void C_USART::Disable(void)
{
  if (MODE_MODBUS_RTU == myMode) {
    if (NULL != myDriverEnable) {
      myDriverEnable->Set(0);   // Receive
    }
    else {
      USART_DECmd(USART1, DISABLE);
    }
  }
  USART_Cmd(myUsart, DISABLE);
}

void C_USART::ReceivedRxByte(void)
{
  uint8_t byte = (uint8_t)USART_ReceiveData(myUsart);
  myRxBuffer.WriteU8(&byte, 1);
  //MSB sind Statusinformationen, MUSS IGNORIERT WERDEN
  return;
}

uint8_t C_USART::SentTxByte(void)
{
  uint8_t byte;
  if (1 == myTxBuffer.Read(&byte, 1)) {
    USART_SendData(myUsart, byte);
    return 1;
  }
  return 0;
}

void C_USART::TransferComplete(void)
{
  if (MODE_MODBUS_RTU == myMode) {
    myTxBuffer.Reset();
  }
  return;
}

void C_USART::ReceiveTimeout(void)
{
  if (MODE_MODBUS_RTU == myMode) {
    if ((0 == myFrameAvailable) && (4 <= myRxBuffer.Count())) {
      myRtuBuffer = myRxBuffer;
      myFrameAvailable = 1;
    }
    myRxBuffer.Reset();
  }

  return;
}

uint16_t C_USART::GetFrame(uint8_t *data, uint16_t length)
{
  uint16_t result = 0;
  
  if ((1 == myFrameAvailable) && (0 < myRtuBuffer.Count()) && (length >= myRtuBuffer.Count())) {
    result = myRtuBuffer.Read(data, length);
    myRtuBuffer.Reset();
    myFrameAvailable = 0;
  }
  return result;
}


uint16_t C_USART::SetFrame(uint8_t *data, uint16_t length)
{
  uint16_t sent = myTxBuffer.WriteU8(data, length);
  if (0 < sent) {
    USART_ITConfig(myUsart, USART_IT_TXE, ENABLE);
  }
  return sent;
}

uint8_t C_USART::FrameAvailable(void)
{
  return myFrameAvailable;
}

void C_USART::Interrupt(void)
{

  if(USART_GetITStatus(USART1, USART_IT_IDLE) != RESET) {
    USART_ClearITPendingBit(myUsart, USART_IT_IDLE);
    myIdleLineDetectionError++;
  }

  if(USART_GetITStatus(USART1, USART_IT_ORE) != RESET) {
    USART_ClearITPendingBit(myUsart, USART_IT_ORE);
    myOverrunError++;
  }

  if(USART_GetITStatus(USART1, USART_IT_PE) != RESET) {
    USART_ClearITPendingBit(myUsart, USART_IT_PE);
    myParityError++;
  }

  if(USART_GetITStatus(USART1, USART_IT_LBD) != RESET) {
    USART_ClearITPendingBit(myUsart, USART_IT_LBD);
    myLinBreakError++;
  }

  if(USART_GetITStatus(USART1, USART_IT_NE) != RESET) {
    USART_ClearITPendingBit(myUsart, USART_IT_NE);
    myNoiseFlagError++;
  }

  if(USART_GetITStatus(USART1, USART_IT_FE) != RESET) {
    USART_ClearITPendingBit(myUsart, USART_IT_FE);
    myFramingError++;
  }

  if(USART_GetITStatus(USART1, USART_IT_RTO) != RESET) {
    USART_ClearITPendingBit(myUsart, USART_IT_RTO);
    ReceiveTimeout();
    myReceiverTimeout++;
  }
  
  if(USART_GetITStatus(USART1, USART_IT_TXE) != RESET) {
    if (NULL != myDriverEnable) {
      myDriverEnable->Set(1);   // Transmit
    }
    if (0 == SentTxByte()) {
      USART_ITConfig(myUsart, USART_IT_TC, ENABLE);
      USART_ITConfig(myUsart, USART_IT_TXE, DISABLE);
    }
    myTxCounter++;
  }

  if(USART_GetITStatus(USART1, USART_IT_TC) != RESET) {
    USART_ClearITPendingBit(myUsart, USART_IT_TC);
    USART_ITConfig(myUsart, USART_IT_TC, DISABLE);
    if (NULL != myDriverEnable) {
      myDriverEnable->Set(0);   // Receive
    }
    TransferComplete();
    myTcCounter++;
  }

  while (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) {
    ReceivedRxByte();
    myRxCounter++;
  }

  return;
}

uint16_t C_USART::Read(uint8_t *data, uint16_t length)
{
  return myRxBuffer.Read(data, length);
}
  
uint16_t C_USART::Write(uint8_t *data, uint16_t length)
{
  uint16_t sent;
  sent = myTxBuffer.WriteU8(data, length);
  if (0 < sent) {
    USART_ITConfig(myUsart, USART_IT_TXE, ENABLE);
  }
  return sent;
}

