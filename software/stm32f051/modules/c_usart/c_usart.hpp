/**
 * \class C_USART
 *
 * \ingroup C_USART
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_USART implementation
 *
 * This class implements the Modbus RTU
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_USART_H
#define C_USART_H

#include <string.h>

#include "stm32f0xx_gpio.h"

#include "c_module/c_module.hpp"
#include "c_buffer/c_buffer.hpp"
#include "c_output/c_output.hpp"

class C_USART : public C_MODULE
{

public:
  enum MODE
  {
    MODE_USART,
    MODE_MODBUS_RTU, 
    NO_MODE
  };

private:
  C_OUTPUT          *myDriverEnable;

  uint16_t          myIdleLineDetectionError;
  uint16_t          myOverrunError;
  uint16_t          myParityError;
  uint16_t          myLinBreakError;
  uint16_t          myNoiseFlagError;
  uint16_t          myFramingError;
  uint16_t          myReceiverTimeout;
  uint16_t          myRxCounter;
  uint16_t          myTxCounter;
  uint16_t          myTcCounter;  // Modbus RTU mode
  C_USART::MODE     myMode;
  C_BUFFER          myRtuBuffer;
  uint8_t           myFrameAvailable;


protected:
  USART_TypeDef*    myUsart;
  C_BUFFER          myRxBuffer;
  C_BUFFER          myTxBuffer;

public:
  /// Create an C_USART
  C_USART(USART_TypeDef* usartx, C_OUTPUT *driverEnable = NULL);

  /// Delete an C_USART
  ~C_USART();

  /// Set the USART in a specific mode
  void SetMode(C_USART::MODE mode);

  /// Enable the interface
  void Enable(void);

  /// Disable the interface
  void Disable(void);

  /// Store a received byte comming from the USART interface
  void ReceivedRxByte(void);

  /// Send a byte to the USART interface
  uint8_t SentTxByte(void);
  
  ///  Transfer completed
  void TransferComplete(void);

  ///  Receive Timeout occoured
  void ReceiveTimeout(void);

  /// Get a received RTU frame
  uint16_t GetFrame(uint8_t *data, uint16_t length);

  /// Send bytes to the USART interface
  uint16_t SetFrame(uint8_t *data, uint16_t length);

  /// Check if a modbus frame is available
  uint8_t FrameAvailable(void);

  /// Interrupt Service Routine
  void Interrupt(void);
  
  /// Read from the RX buffer
  uint16_t Read(uint8_t *data, uint16_t length);
  
  /// Write to the TX buffer
  uint16_t Write(uint8_t *data, uint16_t length);
  
};  // end of class C_USART

#endif  // C_USART_H

