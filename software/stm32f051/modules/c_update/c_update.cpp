#include <stdint.h>
#include <stdio.h> 
#include <stdlib.h> 

/* Includes ------------------------------------------------------------------*/ 
#include "stm32f0xx.h"
#include "stm32f0xx_wwdg.h"

#include "memory_definitions\memory_definitions.h"
#include "c_update\c_update.hpp" 

/* Public function -----------------------------------------------------------*/ 

/**
*/
void C_UPDATE::Flash()
{
  uint32_t  offset = 0;
  uint32_t  offset2 = 0;

  __disable_irq();
  
  FLASH_Unlock();

  while (offset < APP_SIZE) {
    // trigger the watchdog
    WWDG_SetCounter(126);

    if (FLASH_COMPLETE != FLASH_ErasePage(APP_RUNNING + offset)) {
      while(1){};
    }
    offset += PAGE_SIZE; //1K pages
  }

  while (offset2 < APP_SIZE) {
    // trigger the watchdog
    WWDG_SetCounter(126);

    if (FLASH_COMPLETE != FLASH_ProgramWord(APP_RUNNING + offset2, *(uint32_t*)(APP_MIRROR + offset2))) {
      while(1){};
    }
    offset2 += 4;
  }
  
  FLASH_Lock();
  
  NVIC_SystemReset();

  return;
}
