/**
 * \class C_UPDATE
 *
 * \ingroup C_UPDATE
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_UPDATE implementation
 *
 * This class implements the function for the flash update
 *
 * \author Regent Lighting Ltd.
 *
 */
#ifndef __C_MODBUS_H__ 
#define __C_MODBUS_H__  

/** Class description.  
*/ 
class C_UPDATE { 
public: 
  void Flash();
};


#endif 
