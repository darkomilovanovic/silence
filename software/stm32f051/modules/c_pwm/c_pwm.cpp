/** \defgroup C_ADC_COMPONENT C_ADC_COMPONENT
 *
 * \brief Provide some stuff to do stuff
 */

#include "c_pwm/c_pwm.hpp"
#include <stdio.h>

C_PWM::C_PWM(TIM_TypeDef* TIMx, uint32_t frequency, uint8_t timerChannel, uint8_t powerOnLevel, uint8_t defaultPulsePower)
{
  myTimerChannel = timerChannel;
  myTimer.SetDuration(20);
  myTimer.Start();
  myHW_TIMER = TIMx;
  myPWMPowerOnLevel = powerOnLevel;
  myPWMPulsePower = powerOnLevel;
  myDefaultPulsePower = defaultPulsePower;
  myPeriode = (SystemCoreClock / (frequency + 1));
  myPWMDutyCycle = (uint16_t) (((uint32_t) (powerOnLevel) * (myPeriode - 1)) / 100);
  
  if(myTimerChannel == 1)
    TIM_SetCompare1(myHW_TIMER, myPWMDutyCycle);
  else if(myTimerChannel == 2)
    TIM_SetCompare2(myHW_TIMER, myPWMDutyCycle);
  else if (myTimerChannel == 3)
    TIM_SetCompare3(myHW_TIMER, myPWMDutyCycle);
  else if (myTimerChannel == 4)
    TIM_SetCompare4(myHW_TIMER, myPWMDutyCycle);
  else
      {}
}
C_PWM::~C_PWM(void)
{
}

void C_PWM::Start()
{
  myPWMState = PWM_RUNNING;
  myPWMPulsePower = myDefaultPulsePower;
  myPWMDutyCycle = (uint16_t) (((uint32_t) (myPWMPulsePower) * (myPeriode - 1)) / 100);
}

void C_PWM::Start(uint8_t PulsePower)
{
  myPWMState = PWM_RUNNING;
  if(PulsePower > 100)
    myPWMPulsePower = myPWMPowerOnLevel;
  else
    myPWMPulsePower = PulsePower;
  myPWMDutyCycle = (uint16_t) (((uint32_t) (myPWMPulsePower) * (myPeriode - 1)) / 100);
}
 
void C_PWM::SetPowerOnLevel(uint8_t powerOnLevel)
{
  myPWMPowerOnLevel = powerOnLevel;
}
void C_PWM::Stop(void)
{
  myPWMState = PWM_STOP;
  myPWMPulsePower = 0;
  myPWMDutyCycle = 0;
}
  
void C_PWM::SetNewValue(uint8_t PulsePower)
{
  myPWMPulsePower = PulsePower;
  myPWMDutyCycle = (uint16_t) (((uint32_t) (PulsePower) * (myPeriode - 1)) / 100);
}
  
void C_PWM::Enable()
{
  myPWMPulsePower = myPWMPowerOnLevel;
  myPWMDutyCycle = (uint16_t) (((uint32_t) (myPWMPulsePower) * (myPeriode - 1)) / 100);
  
   if(myTimerChannel == 1)
      TIM_SetCompare1(myHW_TIMER, myPWMDutyCycle);
    else if(myTimerChannel == 2)
      TIM_SetCompare2(myHW_TIMER, myPWMDutyCycle);
    else if (myTimerChannel == 3)
      TIM_SetCompare3(myHW_TIMER, myPWMDutyCycle);
    else if (myTimerChannel == 4)
      TIM_SetCompare4(myHW_TIMER, myPWMDutyCycle);
    else
      {}
  TIM_CtrlPWMOutputs(myHW_TIMER, ENABLE);
  myPWMState = PWM_RUNNING;
}

void C_PWM::Disable(void)
{
  /* TIM1 Main Output Enable */
  TIM_CtrlPWMOutputs(myHW_TIMER, DISABLE);
  myPWMState = PWM_NOT_RUNNING;
}

void C_PWM::Handler(void)
{
  if(myTimer.GetState() == C_TIMER::STATE_OVER )
  {
    if(myTimerChannel == 1)
      TIM_SetCompare1(myHW_TIMER, myPWMDutyCycle);
    else if(myTimerChannel == 2)
      TIM_SetCompare2(myHW_TIMER, myPWMDutyCycle);
    else if (myTimerChannel == 3)
      TIM_SetCompare3(myHW_TIMER, myPWMDutyCycle);
    else if (myTimerChannel == 4)
      TIM_SetCompare4(myHW_TIMER, myPWMDutyCycle);
    else
      {}
    myTimer.Restart();
  }
  
}
