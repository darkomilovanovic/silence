/**
 * \class C_HW_SPECIFIC
 *
 * \ingroup C_HW_SPECIFIC
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_HW_SPECIFIC implementation
 *
 * This class implements an adc for the GPIO for the STM32F0xx processor.
 * An input represents a single pin on the processor.
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_PWM_HPP
#define C_PWM_HPP

#include "c_module/c_module.hpp"
#include "c_timer/c_timer.hpp"
#include "stm32f0xx_tim.h"

#define PWM_FREQUENCY_10K 10000
#define PWM_FREQUENCY_100K 100000
#define PWM_FREQUENCY_1M 1000000

enum PWM_STATES{
  PWM_RUNNING,
  PWM_NOT_RUNNING,
  PWM_STOP
};

enum PWM_IDLE{
  IDLE_LOW,
  IDLE_HIGH
};

class C_PWM : C_MODULE
{
public:

  /// Create an C_HW_SPECIFIC

  C_PWM(TIM_TypeDef* TIMx, uint32_t frequency, uint8_t timerChannel, uint8_t powerOnLevel, uint8_t defaultPulsePower);
  ~C_PWM();

  virtual void Start(uint8_t PulsePower);
  void Start(void);
  void SetPowerOnLevel (uint8_t powerOnLevel);
  virtual void Stop(void);
  
  void SetNewValue(uint8_t PulsePower);

  void Enable();
  void Disable(void);
 
  virtual void Handler(void);
  
private:
  C_TIMER          myTimer;
  TIM_TypeDef     *myHW_TIMER;
  uint8_t          myTimerChannel;
  PWM_STATES       myPWMState;
  uint8_t          myDefaultPulsePower;
  uint8_t          myPWMPulsePower;
  uint8_t          myPWMPowerOnLevel;
  uint16_t         myPeriode; //HZ
  uint16_t         myPWMDutyCycle;
  
};  

#endif  // C_PWM_HPP

