/**
 * \class C_INPUT
 *
 * \ingroup C_INPUT
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_OUTPUT implementation
 *
 * This class implements an GPIO output for the STM32F0xx processor.
 * An input represents a single pin on the processor.
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_OUTPUT_H
#define C_OUTPUT_H

#include "stm32f0xx_gpio.h"

#include "c_module/c_module.hpp"


class C_OUTPUT : public C_MODULE
{

private:

protected:
  GPIO_TypeDef      *myPort;
  uint16_t          myPin;

public:

  /// Create an C_OUTPUT
  C_OUTPUT(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

  /// Delete an C_OUTPUT
  ~C_OUTPUT();

  /// Set the state of the output
  void Set(uint8_t action);

  // Get the state of the output
  uint8_t Get(void);

};  // end of class C_OUTPUT

#endif  // C_OUTPUT_H

