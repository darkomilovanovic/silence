/** \defgroup C_OUTPUT C_OUTPUT
 *
 * \brief Provide some stuff to do stuff
 */

#include "c_output/c_output.hpp"
#include <stdio.h>

C_OUTPUT::C_OUTPUT(GPIO_TypeDef* gpioPort, uint16_t gpioPin)
{
  myPort = gpioPort;
  myPin = gpioPin;
}

C_OUTPUT::~C_OUTPUT()
{
  myPort = NULL;
  myPin = 0xFFFF;
}

void C_OUTPUT::Set(uint8_t action)
{
  if (0 < action) {
    GPIO_WriteBit(myPort, myPin, Bit_SET);
  }
  else {
    GPIO_WriteBit(myPort, myPin, Bit_RESET);
  }
  return;
}

uint8_t C_OUTPUT::Get(void)
{
  return GPIO_ReadOutputDataBit(myPort, myPin);
}
