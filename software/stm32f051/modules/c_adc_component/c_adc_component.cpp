/** \defgroup C_ADC_COMPONENT C_ADC_COMPONENT
 *
 * \brief Provide some stuff to do stuff
 */

#include "c_adc_component/c_adc_component.hpp"

C_ADC_COMPONENT::C_ADC_COMPONENT(ADC_TypeDef* ADCDevice)
{
  myADCDevice = ADCDevice;
  myState = ADC_NOT_RUNNING;
}

void C_ADC_COMPONENT::Start(void)
{
  
  /* Enable the ADC peripheral */
  ADC_Cmd(ADC1, ENABLE);  
  
  while (!ADC_GetFlagStatus( myADCDevice, ADC_FLAG_ADRDY ));
    ADC_StartOfConversion(myADCDevice);
  ADC_ClearITPendingBit( myADCDevice, ADC_FLAG_EOSEQ);
  while ( !ADC_GetFlagStatus( myADCDevice, ADC_FLAG_EOSEQ));
  ADC_ClearITPendingBit( myADCDevice, ADC_FLAG_EOSEQ);
  while ( !ADC_GetFlagStatus( myADCDevice, ADC_FLAG_EOSEQ));
    myState = ADC_RUNNING;
  //while (  
}

void C_ADC_COMPONENT::Stop(void)
{
    /* Enable the ADC peripheral */
  ADC_StopOfConversion(myADCDevice);
  
  ADC_Cmd(ADC1, DISABLE);  
  
  myState = ADC_NOT_RUNNING;
}

ADC_STATES C_ADC_COMPONENT::GetStateADC(void)
{
  return myState;
}

void C_ADC_COMPONENT::Handler(void)
{ 
}
