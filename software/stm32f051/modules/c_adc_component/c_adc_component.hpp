/**
 * \class C_ADC_COMPONENT
 *
 * \ingroup C_ADC_COMPONENT
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_ADC_COMPONENT implementation
 *
 * This class implements an adc for the GPIO for the STM32F0xx processor.
 * An input represents a single pin on the processor.
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_ADC_COMPONENT_H
#define C_ADC_COMPONENT_H

#include "stm32f0xx_adc.h"
#include "stm32f0xx_dma.h"

#include "c_adc/c_adc.hpp"

#include "c_module/c_module.hpp"
#include "c_timer/c_timer.hpp"

#define           ADC1_DR_Address                 0x40012440/**< Adresse von der DMA */
#define           NUMBER_OF_MEASURED_AD_CHANNELS  5
#define           OPERATING_VOLTAGE_UPPER_TRESHOLD 3600
#define           OPERATING_VOLTAGE_LOWER_TRESHOLD 3000

enum ADC_STATES {
  ADC_RUNNING,
  ADC_NOT_RUNNING
  };

class C_ADC_COMPONENT : public C_MODULE
{
public:

  /// Create an C_ADC_COMPONENT

  C_ADC_COMPONENT(ADC_TypeDef* ADCDevice);
  /// Delete an C_ADC_COMPONENT
  ~C_ADC_COMPONENT();

  ADC_STATES GetStateADC(void);
  ADC_STATES GetFlagADCUPdated(void);
  void EraseFlagADCUpdated(void);

  virtual void Start(void);
  virtual void Stop(void);
  virtual void Handler(void);
  
private:

  ADC_STATES              myState;
  ADC_TypeDef            *myADCDevice;
  uint16_t               *myPDMA;
  // Adresse von ADC1_DR_Address
  // Anazahl AD_Channels to Measured
};  // end of class C_ADC_COMPONENT

#endif  // C_ADC_COMPONENT_H

