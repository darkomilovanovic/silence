/** \defgroup C_HW_SPECIFIC C_HW_SPECIFIC
 *
 * \brief Do the hardware specific stuff
 */

#include "c_hw_specific/c_hw_specific.hpp"
#include <stdio.h>

C_HW_SPECIFIC::C_HW_SPECIFIC(void)
{
  minStackPointer = UINT32_MAX;
  stackAddress = 0;
}

C_HW_SPECIFIC::~C_HW_SPECIFIC(void)
{
}

uint16_t C_HW_SPECIFIC::GetMinStackPointer(void)
{
  return minStackPointer;
}

void C_HW_SPECIFIC::Reset(void)
{
  NVIC_SystemReset();
}

void C_HW_SPECIFIC::RestartWatchdog(void)
{
 /* Update WWDG counter */
  WWDG_SetCounter(126);
}

void C_HW_SPECIFIC::Start(void)
{
  /* Enable WWDG and set counter value to 127, WWDG timeout = ~341 us * 64 = 21.6 ms 
     In this case the refresh window is: ~683 * (127-80)= 16.5ms < refresh window < ~683 * 64 = 23ms*/
  //RCC_ClearFlag();
  WWDG_Enable(127);
}

void C_HW_SPECIFIC::Stop(void)
{
  return;
}

void C_HW_SPECIFIC::Handler(void)
{
#if defined (DEBUG)
  uint32_t stackPointer;
  
  stackPointer = __get_MSP();
  if(stackPointer < minStackPointer)
  {
    minStackPointer = stackPointer;
  }
#endif
  return;
}

void C_HW_SPECIFIC::DisableInterrupts(void)
{
  __disable_irq();
}
