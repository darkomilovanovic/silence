/**
 * \class C_HW_SPECIFIC
 *
 * \ingroup C_HW_SPECIFIC
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_HW_SPECIFIC implementation
 *
 * This class implements an adc for the GPIO for the STM32F0xx processor.
 * An input represents a single pin on the processor.
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_HW_SPECIFIC_H
#define C_HW_SPECIFIC_H

//#include "core_cm0.h"
#include "c_module/c_module.hpp"
#include "stm32f0xx_wwdg.h"

class C_HW_SPECIFIC : C_MODULE
{
public:

  /// Create an C_HW_SPECIFIC

  C_HW_SPECIFIC();
  ~C_HW_SPECIFIC();

  // Reset the processor for a restart
  void Reset(void);
  
  // Retrigger the watchdog
  void RestartWatchdog(void);
  
  // Return the minimal stack pointer
  uint16_t GetMinStackPointer(void);

  // Initialise the hardware 
  virtual void Start(void);

  // Deinitialise the hardware
  virtual void Stop(void);

  // Do the periodically hardware depending stuff
  virtual void Handler(void);

  // Disable all interrupts
  void DisableInterrupts(void);
  
private:
  uint32_t minStackPointer;
  uint32_t stackAddress;
 
};

#endif  // C_HW_SPECIFIC_H

