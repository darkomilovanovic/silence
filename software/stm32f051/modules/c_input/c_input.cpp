/** \defgroup C_INPUT C_INPUT_PackageTitle
 *
 * \brief Provide some stuff to do stuff
 */

#include "c_input/c_input.hpp"
#include <stdio.h>

C_INPUT::C_INPUT(GPIO_TypeDef* gpioPort, uint16_t gpioPin)
{
  myPort = gpioPort;
  myPin = gpioPin;
}

C_INPUT::~C_INPUT()
{
  myPort = NULL;
  myPin = 0xFFFF;
}

uint8_t C_INPUT::Get(void)
{
  return GPIO_ReadInputDataBit(myPort, myPin);
}
