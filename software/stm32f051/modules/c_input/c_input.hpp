/**
 * \class C_INPUT
 *
 * \ingroup C_INPUT
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * \brief C_INPUT implementation
 *
 * This class implements an GPIO input for the STM32F0xx processor.
 * An input represents a single pin on the processor.
 *
 * \author Regent Lighting Ltd.
 *
 */
 
#ifndef C_INPUT_H
#define C_INPUT_H

#include "stm32f0xx_gpio.h"

#include "c_module/c_module.hpp"


class C_INPUT : public C_MODULE
{

private:

protected:
  GPIO_TypeDef      *myPort;
  uint16_t          myPin;

public:

  /// Create an C_INPUT
  C_INPUT(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

  /// Delete an C_INPUT
  ~C_INPUT();

  /// Receive the state of the input
  uint8_t Get();

};  // end of class C_INPUT

#endif  // C_INPUT_H

